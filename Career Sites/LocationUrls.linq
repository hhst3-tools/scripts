<Query Kind="Statements">
  <Connection>
    <ID>1b4d0ffe-df90-480d-b80e-5cbf12d8f849</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Prod\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <IsProduction>true</IsProduction>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
  <IncludeLinqToSql>true</IncludeLinqToSql>
  <IncludeAspNet>true</IncludeAspNet>
</Query>

var ctx = Connection.GetSystemUserToken(new Guid("3a908be7-bed7-47d3-8d54-c20d47c528a9")).GetCoreContext();

var urls = ctx.Organization.Select(o => new {
 o.OrganizationName,
 o.ExternalName,
 LocationUrl = "https://presbyterianseniorliving.hcshiring.com/jobs?location=" + o.ExternalName.Replace(" ", "%20"),
 LocationWithChildrenUrl = "https://presbyterianseniorliving.hcshiring.com/jobs?location=" + o.ExternalName.Replace(" ", "%20") + "&orgId=" + o.OrganizationId.ToCompactString()
} ).ToArray().Dump();

Util.WriteCsv(urls, @"C:\urls.csv");