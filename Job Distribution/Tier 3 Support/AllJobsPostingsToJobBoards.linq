<Query Kind="Statements">
  <Connection>
    <ID>1b4d0ffe-df90-480d-b80e-5cbf12d8f849</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Prod\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <IsProduction>true</IsProduction>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
  <IncludeLinqToSql>true</IncludeLinqToSql>
</Query>


//Return a List of all jobs posting to indeed.

//Get the database context for the client.
var ctx = Connection.GetSystemUserToken(ClientIds.NorthShoreHealthcareAccount).GetCoreContext();

//Query the LaborRequest table
//WHERE PostToExternalJobBoards = True AND LaborRequestStatus = Open
//Select the OrganizationJob related to that labor request, and store each in a array.
var lrPostedToJobBoard = ctx.LaborRequest.Where(lr => lr.PostToExternalJobBoards && lr.LaborRequestStatus == Vikus.Data.Client.LaborRequestStatus.Open ).Select(lr => lr.OrganizationJob).ToArray();

//An organzation job can contain 0 or many labor request, and a labor request can have 1 OrganizationJob

//From the OrganizationJob table
//Where our array contains the OrganizationJob in the lrPostedToJobBoard array, select that OrganizationJob
//Create a new object with:
//OrganizationNam
//JobName

//This will return the job name and organization name of all the OrganizationJobs tired to open larbore request
var jobs = ctx.OrganizationJob.Where(oj => lrPostedToJobBoard.Contains(oj)).Select(oj => new {
	oj.Organization.OrganizationName,
	oj.Job.JobName

});


DumpReport.WriteCsv(ClientIds.NorthShoreHealthcareAccount, jobs, "jobsPostedToIndeed");