<Query Kind="Statements">
  <Connection>
    <ID>1b4d0ffe-df90-480d-b80e-5cbf12d8f849</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Prod\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <IsProduction>true</IsProduction>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
  <IncludeLinqToSql>true</IncludeLinqToSql>
</Query>

var ctx = Connection.GetSystemUserToken(ClientIds.NorthShoreHealthcareAccount).GetCoreContext();

var lrPostedToJobBoard = ctx.LaborRequest.Where(lr => lr.PostToExternalJobBoards && lr.LaborRequestStatus == Vikus.Data.Client.LaborRequestStatus.Open ).Select(lr => lr.OrganizationJob).ToArray();

var job = ctx.OrganizationJob.Where(oj => lrPostedToJobBoard.Contains(oj)).Select(oj => new {
	oj.Organization.OrganizationName,
	oj.Job.JobName

});


DumpReport.WriteCsv(ClientIds.NorthShoreHealthcareAccount, job, "jobsPostedToIndeed");