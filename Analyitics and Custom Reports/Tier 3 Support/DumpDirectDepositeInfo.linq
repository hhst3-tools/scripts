<Query Kind="Statements">
  <Connection>
    <ID>1b4d0ffe-df90-480d-b80e-5cbf12d8f849</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Prod\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <IsProduction>true</IsProduction>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>

var ctx = Connection.GetSystemUserToken(ClientIds.SolvereSeniorLivingAccount).GetCoreContext();

var orgNames = new List<string> {"Pavilion at Great Hills", "The Monarch at Richardson", "Park Place at Winghaven", "The Hamptons at Meadows Place" };


orgNames.Map(orgName =>
{
	var data = ctx.ApplicationJobOrganization
	.Where(ajo => ajo.Offer.StartDate >= new DateTime(2020, 12, 1))
	.Where(ajo => ajo.Organization.OrganizationName == orgName).AsEnumerable()
	.Select(ajo => new
	{
		ApplicantName = ajo.Application.Candidate.FirstName,
		LastName = ajo.Application.Candidate.LastName,
		ajo.Application.Candidate.SocialSecurityNumber,
		AccountNumber1 = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "AccountNumber1").FirstOrDefault()?.Value,
		RoutingNumber1 = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "RoutingNumber1").FirstOrDefault()?.Value,
		Account1Type = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "AccountType1").FirstOrDefault()?.Value,
		SpecificAmountOrNet = DollarOrPrecent(ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "DirectDepositIsPercent").FirstOrDefault()?.Value),
		Amount = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "DirectDepositAmountOrPercent").FirstOrDefault()?.Value,
		AccountNumber2 = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "AccountNumber2").FirstOrDefault()?.Value,
		RoutingNumber2 = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "RoutingNumber2").FirstOrDefault()?.Value,
		Account2Type = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "AccountType2").FirstOrDefault()?.Value,
		SpecificAmountOrNet2 = DollarOrPrecent(ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "DirectDepositIsPercent2").FirstOrDefault()?.Value),
		Amount2 = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "DirectDepositAmountOrPercent2").FirstOrDefault()?.Value,
	});

	String DollarOrPrecent(String value)
	{
		return value == "False" ? "Specified Amount" : "Net Pay";
	}

	Util.WriteCsv(data, @"C:\code\Reports\Solvere Senior Living\" + orgName + "-ddinfo-12012020-01072020.csv");

});


