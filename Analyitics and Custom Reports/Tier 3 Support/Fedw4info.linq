<Query Kind="Statements">
  <Connection>
    <ID>1b4d0ffe-df90-480d-b80e-5cbf12d8f849</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Prod\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <IsProduction>true</IsProduction>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>

var ctx = Connection.GetSystemUserToken(ClientIds.SolvereSeniorLivingAccount).GetCoreContext();
var orgNames = new List<string> {"Pavilion at Great Hills", "The Monarch at Richardson", "Park Place at Winghaven", "The Hamptons at Meadows Place" };

orgNames.Map(orgName =>
{
var data = ctx.ApplicationJobOrganization
	.Where(ajo => ajo.Offer.StartDate >= new DateTime(2020, 12, 1))
	.Where(ajo => ajo.Organization.OrganizationName == orgName).AsEnumerable()
	.Select(ajo => new
		{

			Location = ajo.Organization.OrganizationName,
	
	
			CandidateFirstName = ajo.Application.Candidate.FirstName,
			CandidateLastName = ajo.Application.Candidate.LastName,
			ajo.Application.Candidate.SocialSecurityNumber,
			ajo.Application.ApplicantGeneral.FullAddress,
			MaritalStatus = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "FederalW4ClaimStatus").FirstOrDefault()?.Value,
			MultipleJobs = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "FederalW4MultipleJobs").FirstOrDefault()?.Value,
			ExemptionsClaimed = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "FederalW4ExemptionsClaimed").FirstOrDefault()?.Value,
			OtherJobs = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "FederalW4OtherJobs").FirstOrDefault()?.Value,
			ClaimedDependents = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "FederalW4NumberOfChildren").FirstOrDefault()?.Value,
			OtherDependents = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "FederalW4OtherDependents").FirstOrDefault()?.Value,
			ExtraWitholding = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "FederalW4ExtraWithhold").FirstOrDefault()?.Value,
			OtherDeductions = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "FederalW4OtherDeductions").FirstOrDefault()?.Value,
			IsExempt = ajo.Application.ApplicationXmlData.ResponseData.ToXml().Elements().Where(node => node.Name.LocalName == "FederalW4IsExempt").FirstOrDefault()?.Value,
	
	}).Dump();

	Util.WriteCsv(data, @"C:\code\Reports\Solvere Senior Living\" + orgName + "-fedW4info-12012020-01072020.csv");

});



