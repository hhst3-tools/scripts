<Query Kind="Statements">
  <Connection>
    <ID>1b4d0ffe-df90-480d-b80e-5cbf12d8f849</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Prod\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <IsProduction>true</IsProduction>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>


//Asign Clinet ID
var clientId = ClientIds.ColavriaHospitalityAccount;
var ctx = Connection.GetSystemUserToken(clientId).GetCoreContext();
var clientName = ctx.Organization.Where(o => o.OrganizationId == clientId).First().OrganizationName;

/*
Request - Return the hartman and jist cores for hires 
within the last two years

All of this data can be accessed through the ApplicationJobOrganization table

The trick to this report is getting the Proccess for the hartman and jist
We need to find the ProcessDefinitionID related to the Process, and pull the
scores out of the process

for the process definition
run ctx.ProcessDefinition.Dump() and look for your Hartman and Jist records
make a note of the ID

*/
//Get Clients Database Connection



//ApplicationJobOrganization Application table w/ job and org data
//ajo.AvailableDate - Date when the application is availible for users to see

var data = ctx.ApplicationJobOrganization.Where(ajo => ajo.AvailableDate.Value.Year <= 2020)
//Start date - The hire date
.Where(ajo => ajo.Offer.StartDate != null)
//Create our table for the data 
.Select(ajo => new {
	ajo.Organization.OrganizationName,
	ajo.Application.Candidate.FirstName,
	ajo.Application.Candidate.LastName,
	
	//Here we used the ProcessDefinitionId we searched for earlier. We call first(), because .Where() will return
	//an IEnumerable (similar to an array or list) of objects even it there is only 1 records
	//.ScoreDisplay will have the score of the assesments
	HartmanScare = ajo.Process.Where(p => p.ProcessDefinitionId == new Guid("a9ba8f1d-2d1e-4c98-81e6-594602d169fb") ).First().ScoreDisplay,
	JistScore = ajo.Process.Where(p => p.ProcessDefinitionId == new Guid("5d0e807e-89f6-4e64-8d68-65b9237c4f2e") ).First().ScoreDisplay
	

}).ToArray().Dump();

Util.WriteCsv(data, @$"C:\code\hhsT3-tools\scripts\Clients\{clientName}\hartmanAndJistScores.csv");
