USE [NSH]

SELECT DISTINCT
	[applicantBio].[Name] AS [Applicant Name],
	[o].[OrganizationName] AS [Location],
	ISNULL(FORMAT([ajo].[AvailableDate], 'MM-dd-yyyy'), '') AS [Pre Application],
	ISNULL(FORMAT([ajo].[ApplicationDate], 'MM-dd-yyy'), '') AS [New Applicant],
	ISNULL(FORMAT([interview].[date], 'MM-dd-yyyy'), ' ') AS [interview Date],
	--Full Reviewed
	ISNULL(FORMAT([offer].[OfferMade], 'MM-dd-yyy'), '') AS [Offer],
	--Aproved for hire
	ISNULL(FORMAT([offer].[StartDate], 'MM-dd-yyy'), '') AS [Hired],
	ISNULL(FORMAT([ajo].[DispositionDate], 'MM-dd-yyy'), '') AS [Disqualified],
	ISNULL([d].[Text], '') AS [Disqualify Reason],
	ISNULL([termInfo].[Terminated], '') AS [Termination Date],
	ISNULL([termInfo].[Termination Reason], '') AS [Termination Reason]
	
	
FROM [ApplicationJobOrganization] [ajo]
INNER JOIN [Organization] [o] ON [ajo].[OrganizationID] = [o].[OrganizationID]
INNER JOIN [Job] [j] on [ajo].[JobID] = [j].[JobID]
INNER JOIN (
	SELECT 
		[a].[ApplicationID],
		[c].[FirstName] + ' ' + [c].[LastName] AS [Name]
	FROM [Application] a
	INNER JOIN [Candidate] [c] ON [a].[ApplicationID] = [c].[CandidateId]
) AS [applicantBio] ON [applicantBio].[ApplicationID] = [ajo].[ApplicationID]


LEFT JOIN [Offer] [offer] on [ajo].[ApplicationJobOrganizationID] = [offer].[ApplicationJobOrganizationId]
LEFT JOIN [Disposition] [d] ON [d].[DispositionID] = [ajo].[DispositionID]

LEFT JOIN (
	SELECT DISTINCT
		[e].[EmployeeId],
		[t].[ChangeDate],
		ISNULL(FORMAT([t].[ChangeDate], 'MM-dd-yyy'), '') AS [Terminated],
		ISNULL([tr].[Reason], '') AS [Termination Reason]
	FROM [Employee] [e]
	INNER JOIN [Termination] [t] ON [t].[EmployeeId] = [e].[EmployeeId]
	INNER JOIN [TermReason] [tr] ON [tr].TermReasonId = [t].[TermReasonId]

) AS [termInfo] ON [termInfo].[EmployeeId] = [applicantBio].[ApplicationID]

LEFT JOIN (
	SELECT DISTINCT
		[p].[ApplicationJobOrganizationID],
		ISNULL([pl].[Timestamp], '' ) AS [date]
	FROM [Process] [p]
	INNER JOIN [ProcessLog] [pl] ON [p].[ProcessID] = [pl].[ProcessId]
	WHERE pl.StatusCode BETWEEN 21 AND 34 AND pl.[Timestamp] >'1900-01-01'
	AND p.ProcessDefinitionID = 'b8600041-b055-4535-89cb-ef3e5d6376c0'
) AS [interview] ON [interview].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID]

where ( [ajo].[DispositionDate] BETWEEN DATEADD(MONTH, -1, GETDATE()) AND GETDATE() ) 
	or ( [ajo].[AvailableDate] BETWEEN DATEADD(MONTH, -1, GETDATE()) AND GETDATE() )
	or ( [ajo].[ApplicationDate] BETWEEN DATEADD(MONTH, -1, GETDATE()) AND GETDATE() )
	or ( [interview].[date] BETWEEN DATEADD(MONTH, -1, GETDATE()) AND GETDATE() )
	or ( [offer].[OfferMade] BETWEEN DATEADD(MONTH, -1, GETDATE()) AND GETDATE() )
	or ( [offer].[StartDate] BETWEEN DATEADD(MONTH, -1, GETDATE()) AND GETDATE() )
	or ( [termInfo].[Terminated] BETWEEN DATEADD(MONTH, -1, GETDATE()) AND GETDATE() )