USE [NSH]

SELECT
	[u].[FirstName] + ' ' + [u].[LastName] AS [User's Name],
	FORMAT([p].[LastLoginDate], 'yyyy-MM-dd') AS [Last Login Date],
	[Touched].[Viewed Count],
	[NotTouched].[Not Viewed Count],
	[NotContacted].[Count] as [Not Contacted]
FROM [User] [u]
INNER JOIN [Person] [p] ON [p].[PersonID] = [u].[PersonID]


OUTER APPLY 
(
	SELECT DISTINCT
		COUNT([ajo].[ApplicationJobOrganizationId]) AS [Viewed Count]
	FROM [ApplicationJobOrganization] [ajo]
	INNER JOIN [ApplicationJobOrganization] [ajoh] on [ajoh].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID] 
											       AND [ajo].[ApplicationDate] BETWEEN DATEADD(MONTH, -1, GETDATE()) AND GETDATE()  
	WHERE [p].[PersonID] = [ajoh].[LastActivityPersonID] 
) As [Touched]


OUTER APPLY 
(
	SELECT DISTINCT
		COUNT([ajo].[ApplicationJobOrganizationId]) AS [Not Viewed Count]
	FROM [ApplicationJobOrganization] [ajo]
	INNER JOIN [ApplicationJobOrganization] [ajoh] on [ajoh].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID]
												   AND [ajoh].[LastActivityPersonID] != [p].[PersonID]
												   AND [ajo].[ApplicationDate] BETWEEN DATEADD(MONTH, -1, GETDATE()) AND GETDATE()
	
) AS [NotTouched]

OUTER APPLY (
	SELECT DISTINCT
		COUNT([a].[ApplicationID]) AS [Count]
	FROM [Application] [a]
	INNER JOIN [ApplicationJobOrganization] [ajo] ON [ajo].ApplicationID = [a].[ApplicationID]
	INNER JOIN [EmailEvent] [e] ON [e].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID] AND [e].[SendingUserPersonID] != [u].[UserID]
	INNER JOIN [Message] [txt] ON [txt].[CandidateId] = [a].[ApplicationID] AND [txt].[SendingUserId] != [u].[UserID]
	WHERE [a].[ApplicationDate] BETWEEN DATEADD(MONTH, -1, GETDATE()) AND GETDATE()
	
) AS [NotContacted]

WHERE [p].[LastLoginDate] IS NOT NULL
ORDER BY [NotTouched].[Not Viewed Count] ASC 
