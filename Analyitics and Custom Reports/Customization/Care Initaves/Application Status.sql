USE [CI]

SELECT DISTINCT
	[applicant].FirstName,
	[applicant].LastName,
	[j].[JobName],
	FORMAT([ajo].[AvailableDate], 'M-dd-yyyy') AS [Date Applied],
	CASE
		WHEN [ajo].[ApplicationStatus] = 1 THEN 'In Process'
		WHEN [ajo].[ApplicationStatus] = 2 THEN 'Ready To Hire'
		WHEN [ajo].[ApplicationStatus] = 3 THEN 'Offered'
		WHEN [ajo].[ApplicationStatus] = 7 THEN 'Disqualified'
		WHEN [ajo].[ApplicationStatus] = 8 THEN 'Employed'
	ELSE '' END AS [Application Status]
	
FROM ApplicationJobOrganization ajo
INNER JOIN Organization o ON ajo.OrganizationID = o.OrganizationID
INNER JOIN Job j ON ajo.JobID = j.JobID
INNER JOIN (
	SELECT
		[a].ApplicationID,
		[c].FirstName,
		[c].LastName
	FROM [Application] [a]
	INNER JOIN [Candidate] [c] ON [a].[ApplicationID] = [c].[CandidateId]


) AS applicant ON applicant.ApplicationID = ajo.ApplicationID 

WHERE [ajo].[AvailableDate] BETWEEN DATEADD(MONTH, -1, GETDATE()) AND GETDATE()
and [ajo].AvailableDate IS NOT NULL