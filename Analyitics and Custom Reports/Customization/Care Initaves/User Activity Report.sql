USE [CI]



SELECT
	[u].[FirstName],
	[u].[LastName],
	FORMAT([p].[LastLoginDate], 'yyyy-MM-dd') AS [Last Login Date],
	[ReviewdResume].[Count] AS [Review Resume],
	[ReviewdApplications].[Count] AS [Review Online Application],
	[PreviousApplications].[Count] AS [Check for Preivous Applications],
	[VeifiedRehire].[Count] AS [Verify Rehire Status],
	[Interviews].[Count] AS [Conduct Interview],
	[ReferenceChecks].[Count] AS [Reference Checks],
	[Offers].[Count] AS [Offers],
	[Hires].[Count] AS [Hires],
	[Emails].[Count] AS [Emails],
	[Text].[Count] AS [Texts]
FROM [User] [u]
INNER JOIN [Person] [p] ON [p].[PersonID] = [u].[PersonID]

OUTER APPLY (
	SELECT DISTINCT
		COUNT([ajo].[ApplicationJobOrganizationID]) AS [Count]
	FROM [ApplicationJobOrganization] [ajo]
		INNER JOIN [Process] [p] ON [p].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID]
		INNER JOIN [ProcessLog] [pl] on [pl].[ProcessId] = [p].[ProcessID]
	WHERE [pl].[UserId] = [u].[UserID]
		AND [pl].StatusCode = 1
		AND [pl].[Timestamp] BETWEEN DATEADD(WEEK, -1, GETDATE()) AND GETDATE()
		AND [p].[ProcessDefinitionID] = '4cf4b4be-c615-4b6a-be7f-ce8b2531e0bf'
) AS [ReviewdResume]

OUTER APPLY (
	SELECT DISTINCT
		COUNT([ajo].[ApplicationJobOrganizationID]) AS [Count]
	FROM [ApplicationJobOrganization] [ajo]
		INNER JOIN [Process] [p] ON [p].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID]
		INNER JOIN [ProcessLog] [pl] on [pl].[ProcessId] = [p].[ProcessID]
	WHERE [pl].[UserId] = [u].[UserID]
		AND [pl].StatusCode = 1
		AND [pl].[Timestamp] BETWEEN DATEADD(WEEK, -1, GETDATE()) AND GETDATE()
		AND [p].[ProcessDefinitionID] = 'b93ab2c1-fb37-4a13-98a9-180bffbc0a0a'
) AS [ReviewdApplications]

OUTER APPLY (
	SELECT
		COUNT([pl].[UserId]) AS [Count]
	FROM [ApplicationJobOrganization] [ajo]
		INNER JOIN [Process] [p] ON [p].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID]
		INNER JOIN [ProcessLog] [pl] on [pl].[ProcessId] = [p].[ProcessID]
	WHERE [pl].[UserId] = [u].[UserID]
		AND [pl].StatusCode = 1
		AND [pl].[Timestamp] BETWEEN DATEADD(WEEK, -1, GETDATE()) AND GETDATE()
		AND [p].[ProcessDefinitionID] = 'ffc9bd1c-d20e-42a0-98dc-a9650392ef73'
) AS [PreviousApplications]

OUTER APPLY (
	SELECT
		COUNT([ajo].[ApplicationJobOrganizationID]) AS [Count]
	FROM [ApplicationJobOrganization] [ajo]
		INNER JOIN [Process] [p] ON [p].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID]
		INNER JOIN [ProcessLog] [pl] on [pl].[ProcessId] = [p].[ProcessID]
	WHERE [pl].[UserId] = [u].[UserID]
		AND [pl].StatusCode = 1
		AND [pl].[Timestamp] BETWEEN DATEADD(WEEK, -1, GETDATE()) AND GETDATE()
		AND [p].[ProcessDefinitionID] = 'a6353682-e1e2-4465-98ee-67613a700df4'
) AS [VeifiedRehire]

OUTER APPLY (
	SELECT
		COUNT([ajo].[ApplicationJobOrganizationID]) AS [Count]
	FROM [ApplicationJobOrganization] [ajo]
		INNER JOIN [Process] [p] ON [p].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID]
		INNER JOIN [ProcessLog] [pl] on [pl].[ProcessId] = [p].[ProcessID]
	WHERE [pl].[UserId] = [u].[UserID]
		AND [pl].StatusCode = 1
		AND [pl].[Timestamp] BETWEEN DATEADD(WEEK, -1, GETDATE()) AND GETDATE()
		AND [p].[ProcessDefinitionID] = '295b3c15-a02e-4c36-a682-763858ef6096'
) AS [Interviews]

OUTER APPLY (
	SELECT
		COUNT([ajo].[ApplicationJobOrganizationID]) AS [Count]
	FROM [ApplicationJobOrganization] [ajo]
		INNER JOIN [Process] [p] ON [p].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID]
		INNER JOIN [ProcessLog] [pl] on [pl].[ProcessId] = [p].[ProcessID]
	WHERE [pl].[UserId] = [u].[UserID]
		AND [pl].StatusCode = 1 
		AND [pl].[Timestamp] BETWEEN DATEADD(WEEK, -1, GETDATE()) AND GETDATE()
		AND [p].[ProcessDefinitionID] = '0faf08cf-5c4e-445a-b3e4-1ff267a90d1d'
) AS [ReferenceChecks]

OUTER APPLY (
	SELECT
		COUNT([ajo].[ApplicationJobOrganizationID]) AS [Count]
	FROM [ApplicationJobOrganization] [ajo]
	INNER JOIN [ApplicationJobOrganizationHistory] [ah] ON [ah].[ApplicationJobOrganizationID] = [ajo].ApplicationJobOrganizationID
	INNER JOIN [Offer] [o] ON [o].[ApplicationJobOrganizationId] = [ajo].[ApplicationJobOrganizationID]
	WHERE [ah].ApplicationStatus = 3
	AND [ah].[HistoryStartDate] BETWEEN DATEADD(WEEK, -1, GETDATE()) AND GETDATE()
	AND [o].[StartDate] IS NULL
	AND [p].[PersonID] = [ah].[LastActivityPersonID]
) AS [Offers]

OUTER APPLY (
	SELECT DISTINCT
		COUNT([ajo].[ApplicationJobOrganizationID]) AS [Count]
	FROM [ApplicationJobOrganization] [ajo]
	INNER JOIN [ApplicationJobOrganizationHistory] [ah] ON [ah].[ApplicationJobOrganizationID] = [ajo].ApplicationJobOrganizationID
	INNER JOIN [Offer] [o] ON [o].[ApplicationJobOrganizationId] = [ajo].[ApplicationJobOrganizationID]
	WHERE [ah].ApplicationStatus = 8
	AND [o].[StartDate] IS NOT NULL
	AND [o].[StartDate] BETWEEN DATEADD(WEEK, -1, GETDATE()) AND GETDATE()
	AND [p].[PersonID] = [ah].[LastActivityPersonID]
) AS [Hires]

OUTER APPLY (
	SELECT DISTINCT
		COUNT([ajo].[ApplicationJobOrganizationID]) AS [Count]
	FROM [ApplicationJobOrganization] [ajo]
	INNER JOIN [Message] [m] ON [m].[CandidateId] = [ajo].[ApplicationID]
	WHERE [m].[SendingUserId] = [u].[UserID]
	AND [m].[Timestamp] BETWEEN DATEADD(WEEK, -1, GETDATE()) AND GETDATE()
) AS [Text]

OUTER APPLY (
	SELECT
		COUNT([ajo].[ApplicationJobOrganizationID]) AS [Count]
	FROM [EmailEvent] [e]
	INNER JOIN [ApplicationJobOrganization] [ajo] ON [e].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID]
	WHERE [ajo].[AvailableDate] BETWEEN DATEADD(WEEK, -1, GETDATE()) AND GETDATE()
	AND [e].[SendingUserPersonID] = [p].[PersonID]
) AS [Emails]

ORDER BY [p].[LastLoginDate] DESC