USE [PRESBYTERIANSENIORLIVING]
SELECT
	[o].[OrganizationName],
	[j].[JobName],
	ISNULL((CAST([lr].[RequestYear] AS varchar) + '-' + RIGHT('0000' + CAST([lr].[RequestNumber] AS varchar),5)), '') AS RequestId,
	ISNULL([lr].[CustomInfo].value('(/custominfo/PositionNumber)[1]', 'varchar'), '') AS [PositionNumber],
	ISNULL([lr].[CustomInfo].value('(/custominfo/FLSAStatus)[1]', 'varchar'), '') AS [FLSAStatus],
	ISNULL([lr].[CustomInfo].value('(/custominfo/FTE)[1]', 'varchar'), '') AS [FTE],
	ISNULL([lr].[CustomInfo].value('(/custominfo/JobTitleShift)[1]', 'varchar'), '') AS [JobTitleShift],
	ISNULL([lr].[CustomInfo].value('(/custominfo/ReplacementFor)[1]', 'varchar'), '') AS [ReplacementFor],
	ISNULL(FORMAT([lr].[RequestDate], 'MM-dd-yyyy'), '') AS [CreatedDate]
	
FROM [OrganizationJob] [oj]
INNER JOIN [LaborRequest] [lr] ON [lr].[OrganizationJobID] = [oj].[OrganizationJobID]
INNER JOIN [Organization] [o] ON [o].[OrganizationID] = [oj].[OrganizationID]
INNER JOIN [Job] [j] ON [j].[JobID] = [oj].[JobID]

WHERE [lr].[LaborRequestStatus] = 2



