<Query Kind="Statements">
  <Connection>
    <ID>1b4d0ffe-df90-480d-b80e-5cbf12d8f849</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Prod\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <IsProduction>true</IsProduction>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>

var ctx = Connection.GetSystemUserToken(ClientIds.ChristianLivingCommunitiesAccount).GetCoreContext();
var activeAjo = ctx.ApplicationJobOrganization.Where(ajo => ajo.IsActive ).AsEnumerable();

var quickApply = activeAjo.Where(ajo => ajo.IsActive)
.Where(ajo => ajo.AvailableDate != null && ajo.ApplicationDate == null )
.Where(ajo => ajo.ApplicationStatus != Vikus.Data.Client.ApplicationStatus.Disqualified)
.Select(ajo => new {
    ajo.Organization.OrganizationName,
	ajo.Job.JobName,
	ApplicationStatus = "Incomplete"
} ).ToList();

var completedApps = activeAjo.Where(ajo => ajo.ApplicationDate != null)
.Where(ajo => ajo.ApplicationStatus == Vikus.Data.Client.ApplicationStatus.InProcess)
.Select(ajo => new {
    ajo.Organization.OrganizationName,
	ajo.Job.JobName,
	ApplicationStatus = "Complete"

} ).GroupBy(ajo => (ajo.OrganizationName, ajo.JobName)).AsEnumerable().Dump();

//var data = new List<dynamic>();
//
//quickApply.ForEach(a => data.Add(a));
//completedApps.ForEach(a => data.Add(a));
//
//DumpReport.WriteCsv(ClientIds.ChristianLivingCommunitiesAccount, data, "quickApplyAndCompleteApplications.csv")






