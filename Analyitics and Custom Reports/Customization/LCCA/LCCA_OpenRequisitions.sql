USE [LCCA]

SELECT 
*
INTO #v_laborRequest
FROM LaborRequest

SELECT
*
INTO #v_offers
FROM Offer


SELECT
	[ParentOrg2].[OrganizationName] AS [Division]
   ,[ParentOrg1].[OrganizationName] AS [Region]
   ,[org].[OrganizationName] AS [LocationName]
   ,[jt].[Name] AS [Job Category]
   ,[j].[JobName] AS [Job Title]
    ,CASE
		WHEN ISNULL([ReqCount].[count], 0) > 0 THEN [ReqCount].[count]
	END AS [Total # of Open Requisitions]
   ,ISNULL([TotalApplicantCount].[count], 0) AS [Total # All Applicants]
   ,ISNULL([TotalActiveApplicantCount].[count], 0) AS [Total # Active Applicants]
   ,FORMAT([DatesJobPosted].[DateLastPosted], 'MM-dd-yyyy') AS [Last Posted Date]
   ,[DaysAged].[DaysPostingAged] AS [Days Aged (Since last Posted)]
   ,DATEDIFF(DAY, [DatesJobPosted].[DateLastPosted], GETDATE()) AS [Total Days (Since last Posted)]
   ,ISNULL([ReqWithOffersCount].[count], 0) AS [Offers Sent (Since last Posted)]
   ,ISNULL([OffersAccepted].[count], 0) AS [Offers Accepted (Since last Posted)]

FROM [Job] AS j
INNER JOIN [JobType] jt
	ON jt.JobTypeID = j.JobTypeID
INNER JOIN [OrganizationJob] oj
	ON [oj].JobID = j.JobID
INNER JOIN [Organization] org
	ON org.[OrganizationID] = [oj].[OrganizationID]
INNER JOIN [Organization] ParentOrg1
	ON org.ParentOrgID = ParentOrg1.[OrganizationID]
INNER JOIN [Organization] ParentOrg2
	ON ParentOrg1.ParentOrgID = ParentOrg2.[OrganizationID]
INNER JOIN [ApplicationJobOrganization] AS ajo
	ON [ajo].JobID = [oj].JobID

INNER JOIN (SELECT
		[lr].[OrganizationJobID]
	   ,COUNT(1) AS [Count]
	FROM [#v_laborRequest] [lr] WITH (NOLOCK)
	WHERE [lr].[LaborRequestStatus] = 2
	GROUP BY [lr].[OrganizationJobID]) AS ReqCount
	ON [ReqCount].[OrganizationJobID] = [oj].[OrganizationJobID]

INNER JOIN (
	SELECT
		[oj].OrganizationJobId,
		MAX([p].[PostedOn]) AS [DateLastPosted]
	FROM [OrganizationJob] [oj]
	INNER JOIN [Posting] [p]  ON [p].OrganizationJobId = [oj].[OrganizationJobId]
	INNER JOIN [LaborRequest] [lr] ON [lr].[OrganizationJobId] = [oj].[OrganizationJobId] AND [lr].[LaborRequestStatus] = 2
	GROUP BY [oj].[OrganizationJobId], [lr].[PostingTitle]
	) AS DatesJobPosted ON [DatesJobPosted].[OrganizationJobID] = [oj].[OrganizationJobID]

INNER JOIN (SELECT
		[lr].[OrganizationJobID]
	   ,CASE
			WHEN DATEDIFF(DAY, [lastposting].[lastDatePosted], GETDATE()) BETWEEN 0 AND 30 THEN '0-30 days'
			WHEN DATEDIFF(DAY, [lastposting].[lastDatePosted], GETDATE()) BETWEEN 31 AND 60 THEN '31-60 days'
			WHEN DATEDIFF(DAY, [lastposting].[lastDatePosted], GETDATE()) BETWEEN 61 AND 90 THEN '61-90 days'
			WHEN DATEDIFF(DAY, [lastposting].[lastDatePosted], GETDATE()) >= 91 THEN '91+ days'
		END AS [DaysPostingAged]
	FROM [#v_laborRequest] [lr] WITH (NOLOCK)
	INNER JOIN (
		SELECT
			[p].[OrganizationJobId],
			MAX(P.PostedOn) AS [lastDatePosted]
		FROM [Posting] [p]
		GROUP BY [p].[OrganizationJobId]
	) AS lastposting ON [lastposting].[OrganizationJobId] = [lr].[OrganizationJobId]
		WHERE [lr].[LaborRequestStatus] = 2
		GROUP BY [lr].[LaborRequestID],
				[lastposting].[lastDatePosted]
				,[lr].[OrganizationJobId]
				,[lr].[PostingTitle]


) AS [DaysAged]	ON [DaysAged].[OrganizationJobID] = [DatesJobPosted].[OrganizationJobID]

LEFT JOIN (SELECT
		[lrs].[OrganizationJobID]
	   ,COUNT(1) AS [Count]
	FROM [#v_offers] [off] WITH (NOLOCK)
	INNER JOIN (
		SELECT
			[lr].[LaborRequestID]
		   ,[lr].[OrganizationJobID]
		   ,MAX([p].[PostedOn]) AS [LatestOpening]
		FROM [#v_laborRequest] [lr]
		INNER JOIN [Posting] [p] ON [p].[OrganizationJobId] = [lr].[OrganizationJobId]
		GROUP BY [lr].[LaborRequestID]
				,[lr].[OrganizationJobId]
				,[lr].[PostingTitle]
	) AS [lrs] ON [off].[LaborRequestID] = [lrs].[LaborRequestID]

	WHERE [off].[JobOfferDate] IS NOT NULL
	AND [off].[JobOfferDate] BETWEEN [lrs].[LatestOpening] AND GETDATE()
	GROUP BY [lrs].[OrganizationJobID]) AS ReqWithOffersCount
	ON [ReqWithOffersCount].[OrganizationJobID] = [DaysAged].[OrganizationJobID]


LEFT JOIN (
	SELECT
		[lrs].[OrganizationJobID]
	   ,COUNT(1) AS [Count]
	FROM [#v_offers] [off] WITH (NOLOCK)
	INNER JOIN (
		SELECT
			[lr].[LaborRequestID]
		   ,[lr].[OrganizationJobID]
		   ,MAX([p].[PostedOn]) AS [LatestOpening]
		FROM [#v_laborRequest] [lr]
		INNER JOIN [Posting] [p] ON [p].[OrganizationJobId] = [lr].[OrganizationJobId]
		GROUP BY [lr].[LaborRequestID]
				,[lr].[OrganizationJobId]
				,[lr].[PostingTitle]
	) AS [lrs] ON [off].[LaborRequestID] = [lrs].[LaborRequestID]

	WHERE [off].[AcceptanceDate] IS NOT NULL
	AND [off].[AcceptanceDate] BETWEEN [lrs].[LatestOpening] AND GETDATE()
	GROUP BY [lrs].[OrganizationJobID]) AS OffersAccepted
	ON [OffersAccepted].[OrganizationJobID] = [ReqWithOffersCount].[OrganizationJobID]

OUTER APPLY (
	SELECT
		COUNT([ajo].[ApplicationJobOrganizationID]) AS [Count]
	FROM Application a
	LEFT JOIN [ApplicationJobOrganization] ajo ON [a].[ApplicationID] = [ajo].[ApplicationID]
	WHERE [ajo].[JobID] = [j].[JobID]
	AND [ajo].[OrganizationID] = [org].[OrganizationID]
	AND [ajo].[AvailableDate] IS NOT NULL
	AND [ajo].[DispositionDate] IS NULL

	GROUP BY [ajo].[JobID]
			,[ajo].[OrganizationID]
) AS [TotalApplicantCount]

OUTER APPLY (
	SELECT
		COUNT([ajo].[ApplicationJobOrganizationID]) AS [Count]
	FROM [Application] a
	LEFT JOIN [ApplicationJobOrganization] ajo
		ON [a].[ApplicationID] = [ajo].[ApplicationID]
	WHERE [ajo].[JobID] = [j].[JobID]
	AND [ajo].[OrganizationID] = [org].[OrganizationID]
	AND [ajo].[AvailableDate] IS NOT NULL
	AND [ajo].[DispositionDate] IS NULL
	AND [ajo].[IsActive] = 1
	AND [ajo].[ApplicationStatus] != 0
	AND [ajo].[ApplicationStatus] != 7
	AND [ajo].[ApplicationStatus] != 8

	GROUP BY [ajo].[JobID]
			,[ajo].[OrganizationID]
	) AS [TotalActiveApplicantCount]



GROUP BY [ParentOrg1].[OrganizationName]
		,[ParentOrg2].[OrganizationName]
		,[org].[OrganizationName]
		,[j].[JobName]
		,[jt].[Name]
		,[ReqCount].[count]
		,[TotalApplicantCount].[count]
		,[TotalActiveApplicantCount].[count]
		,[DatesJobPosted].[DateLastPosted]
		
		,[DaysAged].[DaysPostingAged]
		,[ReqWithOffersCount].[count]
		,[OffersAccepted].[count]