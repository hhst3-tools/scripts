<Query Kind="Statements">
  <Connection>
    <ID>1b4d0ffe-df90-480d-b80e-5cbf12d8f849</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Prod\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <IsProduction>true</IsProduction>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>

var ctx = Connection.GetSystemUserToken(ClientIds.MorningPointeAccount).GetCoreContext();

var ajos = ctx.ApplicationJobOrganization;

var  complete2019Applicants = ajos
	.Where(ajo => ajo.ApplicationDate >= new DateTime(2019, 01, 01) )
	.Where(ajo => ajo.ApplicationDate < new DateTime(2020, 01, 01) );

var complete2020Applicants = ajos
	.Where(ajo => ajo.ApplicationDate >= new DateTime(2020, 01, 01))
	.Where(ajo => ajo.ApplicationDate < new DateTime(2021, 01, 01));

var incomplete2019Applicants = ajos
	.Where(ajo => ajo.ApplicationDate == null)
	.Where(ajo => ajo.ApplicationStartDate >= new DateTime(2019, 01, 01))
	.Where(ajo => ajo.ApplicationStartDate < new DateTime(2020, 01, 01));

var incomplete2020Applicants = ajos
	.Where(ajo => ajo.ApplicationDate == null)
	.Where(ajo => ajo.ApplicationStartDate >= new DateTime(2020, 01, 01))
	.Where(ajo => ajo.ApplicationStartDate < new DateTime(2021, 01, 01));



var complete2019Data = complete2019Applicants.Select(a => new
{
	ApplicantName = @$"{a.Application.Candidate.FirstName} {a.Application.Candidate.LastName}",
	ApplicationSubmittedDate = a.ApplicationDate,
	a.Organization.OrganizationName,
	a.Referrer
} );

Util.WriteCsv(complete2019Data.ToArray(), @"C:\code\Reports\Morning Pointe\complete2019.csv");

var complete2020Data = complete2020Applicants.Select(a => new
{
	ApplicantName = @$"{a.Application.Candidate.FirstName} {a.Application.Candidate.LastName}",
	ApplicationSubmittedDate = a.ApplicationDate,
	a.Organization.OrganizationName,
	a.Referrer
});

Util.WriteCsv(complete2020Data, $@"C:\code\Reports\Morning Pointe\complete2020.csv");

var incomplete2020Data = incomplete2020Applicants.Select(a => new
{
	ApplicantName = @$"{a.Application.Candidate.FirstName} {a.Application.Candidate.LastName}",
	ApplicationStartDate = a.ApplicationStartDate,
	a.Organization.OrganizationName,
	a.Referrer
});


Util.WriteCsv(incomplete2020Data, $@"C:\code\Reports\Morning Pointe\incomplete2020.csv");

var incomplete2019Data = incomplete2019Applicants.Select(a => new
{
	ApplicantName = @$"{a.Application.Candidate.FirstName} {a.Application.Candidate.LastName}",
	ApplicationStartDate = a.ApplicationStartDate,
	a.Organization.OrganizationName,
	a.Referrer
});

Util.WriteCsv(incomplete2019Data, $@"C:\code\Reports\Morning Pointe\incomplete2019.csv");

var complete2019SourceMap = genSourceCounts("complete2019-sources", complete2019Applicants).ToArray();
var complete2020SourceMap = genSourceCounts("complete2020-sources", complete2020Applicants).ToArray();
var incomplete2019SourceMap = genSourceCounts("incomplete2019-sources", incomplete2019Applicants).ToArray();
var incomplete2020SourceMap = genSourceCounts("incomplete2020-sources", incomplete2020Applicants).ToArray();

Dictionary<String, int> genSourceCounts(String file, IEnumerable<Vikus.Data.Client.ApplicationJobOrganization> ajos)
{
	var sourceMap = new Dictionary<String, int>();
	var totalApps = ajos.Count();
	
	ajos.Map(a =>
	 {
		 if (!a.Referrer.IsEmpty())
		 {
			 if (!sourceMap.ContainsKey(a.Referrer))
			 {
				 sourceMap.Add(a.Referrer, 1);
			 }
			 else
			 {
				 var newTotal = sourceMap[a.Referrer] + 1;
				 sourceMap[a.Referrer] = newTotal;
			 }
		 }
		 else
		 {
			 if (!sourceMap.ContainsKey("unknown"))
			 {
				 sourceMap.Add("unknown", 1);
			 }
			 else
			 {
				 var newTotal = sourceMap["unknown"] + 1;
				 sourceMap["unknown"] = newTotal;
			 }
		 }
	 });
	
	sourceMap.Add("totalApps", totalApps);
	Util.WriteCsv(sourceMap.ToArray(), $@"C:\code\Reports\Morning Pointe\{file}.csv");
	return sourceMap;
}


