USE [CHRISTIANLIVINGCOMMUNITIES]

SELECT
	[o].[OrganizationName],
	[j].[JobName],
	SUM(ISNULL([quickApply].[Count], 0)) AS [Quick Apply Count],
	SUM(ISNULL([completedApplication].[Count], 0)) AS [Completed Application Count]
FROM [ApplicationJobOrganization] [ajo]
INNER JOIN [Organization] [o] ON [o].[OrganizationID] = [ajo].[OrganizationID] 
INNER JOIN [Job] [j] ON [j].[JobID]  = [ajo].[JobID]




LEFT JOIN (
   SELECT
      [ajo].[ApplicationJobOrganizationID],
	  COUNT(1) AS [Count]
	FROM ApplicationJobOrganization ajo
	WHERE ajo.AvailableDate IS NOT NULL AND ajo.ApplicationDate = NULL
	AND ajo.AvailableDate >= DATEADD(DAY, -7, GETDATE())  
	AND ajo.ApplicationStatus = 1

	
	GROUP BY [ajo].[ApplicationJobOrganizationID]
) as [quickApply] ON [quickApply].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID] 

LEFT JOIN (
   SELECT
      ajo2.ApplicationJobOrganizationID, 
	  COUNT(1) AS [Count]
	FROM ApplicationJobOrganization ajo2
	WHERE ajo2.AvailableDate IS NOT NULL AND ajo2.ApplicationDate IS NOT NULL 
	AND ajo2.ApplicationDate >= DATEADD(DAY, -7, GETDATE())
	AND ajo2.IsActive = 1
	and ajo2.ApplicationStatus  = 1
	
	
	GROUP BY [ajo2].[ApplicationJobOrganizationID]
) as [completedApplication] ON [completedApplication].ApplicationJobOrganizationID = [ajo].ApplicationJobOrganizationID 


GROUP BY 	[o].[OrganizationName], [j].[JobName]