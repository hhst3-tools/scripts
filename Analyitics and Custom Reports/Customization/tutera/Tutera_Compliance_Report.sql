USE [TSLA]
        

SELECT DISTINCT
  [c].[FirstName] + ' ' + [c].[LastName] AS [Applicant Name],
  [o].[OrganizationName],
  [o].[State],
  FORMAT([off].[StartDate], 'MM-dd-yyyy') AS [Start Date],
  [il_arb].[SignatureDate] 

FROM [ApplicationJobOrganization] [ajo]
INNER JOIN [Organization] [o] ON [ajo].[OrganizationID] = [o].[OrganizationID]
INNER JOIN [Application] [a] ON [ajo].[ApplicationID] = [a].[ApplicationID]
INNER JOIN [Candidate] [c] ON [ajo].[ApplicationID] = [c].[CandidateId]
INNER JOIN [Offer] [off] ON [ajo].[ApplicationJobOrganizationID] = [off].[ApplicationJobOrganizationId]
INNER JOIN [Process] [p] ON [ajo].[ApplicationJobOrganizationID] = [p].[ApplicationJobOrganizationID]

LEFT JOIN (
	SELECT
		[pf].[ProcessFormId],
		[pf].[ProcessId],
		[pf].[SignatureDate]
	FROM  [ProcessForm] [pf]
	INNER JOIN [OnboardingPDFFormVersion] [version] ON [version].[OnboardingPDFFormVersionID] = [pf].[OnboardingPDFVersionId]
	INNER JOIN [OnboardingPDFForm] [pdf] ON [pdf].[OnboardingPDFFormID] = [version].[OnboardingPDFFormID]
	WHERE [pdf].[Name] = 'Arbitration and Class Action Waiver (IL)'
) AS [il_arb] ON  [il_arb].[ProcessId] = [p].[ProcessId]

WHERE [o].[State] = 'IL' 
OR [o].[State] = 'TX'
AND [il_arb].[SignatureDate] IS NOT NULL


