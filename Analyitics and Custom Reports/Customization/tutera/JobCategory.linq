<Query Kind="Statements">
  <Connection>
    <ID>1b4d0ffe-df90-480d-b80e-5cbf12d8f849</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Prod\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <IsProduction>true</IsProduction>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>

var ctx = Connection.GetSystemUserToken(ClientIds.TuteraSeniorLivingAccount).GetCoreContext();

ctx.Organization.AsEnumerable().Where(o => o.OrganizationName.Contains(""));

var orgs = new List<Guid?>() {
	new Guid("cad8399c-441d-40ba-acce-aa65a1dd4aea"), //Region 3
	new Guid("7bca25c9-b242-4511-970a-db01485b9095"), //Region 4
	new Guid("a7757430-f0b4-41bb-afca-6073f994d786"), //Metropolis
	new Guid("63cca4ce-d384-4efc-9f58-d8f0276c9e74"), //The Waterford at Bridle Brook
	new Guid("4484134e-e54f-45f3-a0eb-910b93075c4d"), //Windsor Care Center
	
};

var excludeOrg = new List<Guid?>() {
	new Guid("b37cad03-e521-4e4a-990a-3b306df5d486"), // Griswold Rehabilitation & Health Care Center
	new Guid("2616699f-5b0e-4f81-9862-56ac32cd1783"), // Griswold Rehabilitation & Health Care Center
};
	



ctx.ApplicationJobOrganization
.Where(ajo => ajo.ApplicationStatus == Vikus.Data.Client.ApplicationStatus.Employed )
.Where( ajo => orgs.Contains(ajo.Organization.OrganizationId) || orgs.Contains(ajo.Organization.ParentOrgId))
.Where( ajo => !excludeOrg.Contains(ajo.OrganizationId)) 
.Where(ajo => ajo.Offer.StartDate >= new DateTime(2019, 6, 1) )
.Select(ajo => new {
    ajo.Application.Candidate.FirstName,
	ajo.Application.Candidate.LastName,
	ajo.Organization.OrganizationName,
	ILSignedArbitrationWaiver = ajo.Process.Select(p => p.ProcessForm.Select(pf => pf.OnboardingPdfVersion)).Where( v => v.Where( x => x.OnboardingPdfForm.Name == "Arbitration and Class Action Waiver (IL)") ).Select(v => v)
} ).Dump();