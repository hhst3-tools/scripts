<Query Kind="Statements">
  <Connection>
    <ID>1b4d0ffe-df90-480d-b80e-5cbf12d8f849</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Prod\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <IsProduction>true</IsProduction>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
  <Namespace>Vikus.Data.Client</Namespace>
</Query>

var ctx = Connection.GetSystemUserToken(ClientIds.TuteraSeniorLivingAccount).GetCoreContext();


var orgs = new List<Guid?>() {
	new Guid("cad8399c-441d-40ba-acce-aa65a1dd4aea"), //Region 3
	new Guid("7bca25c9-b242-4511-970a-db01485b9095"), //Region 4
	new Guid("a7757430-f0b4-41bb-afca-6073f994d786"), //Metropolis
	new Guid("63cca4ce-d384-4efc-9f58-d8f0276c9e74"), //The Waterford at Bridle Brook
	new Guid("4484134e-e54f-45f3-a0eb-910b93075c4d"), //Windsor Care Center	
};

var excludeOrg = new List<Guid?>() {
	new Guid("b37cad03-e521-4e4a-990a-3b306df5d486"), // Griswold Rehabilitation & Health Care Center
	new Guid("2616699f-5b0e-4f81-9862-56ac32cd1783"), // Griswold Rehabilitation & Health Care Center
};
	
var includedOrgs = ctx.Organization.Where(o =>orgs.Contains(o.OrganizationId) || orgs.Contains(o.ParentOrgId) )
.Where(o => excludeOrg.Contains(o.OrganizationId));

var ajos = ctx.ApplicationJobOrganization
.Where(ajo => ajo.AvailableDate > new DateTime(2019, 1, 1)  )
.Where(ajo => ajo.Offer.StartDate.Value >= new DateTime(2019, 6, 1));

var pdfReview = ctx.PdfFormReview.Where(x => x.Application.ApplicationJobOrganization.Any(y => ajos.Contains(y)));

var data = pdfReview.Where( x =>	
	x.OnboardingPdfFormVersion.OnboardingPdfForm.Name == "Arbitration and Class Action Waiver (IL)" ||
	x.OnboardingPdfFormVersion.OnboardingPdfForm.Name == "Arbitration and Class Action Waiver (TX)" ||
	x.OnboardingPdfFormVersion.OnboardingPdfForm.Name == "Written Release for Biometric Timekeeping (IL)" ||
	x.OnboardingPdfFormVersion.OnboardingPdfForm.Name == "Written Release for Biometric Timekeeping (TX)"
).Select(x => new {
			x.Application.Candidate.FirstName,
			x.Application.Candidate.LastName,
			Location = x.Application.ApplicationJobOrganization.Where(ajo => ajo.Application.Candidate == x.Application.Candidate ).FirstOrDefault().Organization.OrganizationName,
			StartDate = x.Application.ApplicationJobOrganization.Select(y => y.Offer.StartDate ).FirstOrDefault().ToShortDateString(),
			FormName = x.OnboardingPdfFormVersion.OnboardingPdfForm.Name,
			x.ReviewedOn
		})
		.ToArray().Dump();
		
		
		
Util.WriteCsv(data, @"C:\code\tutera-complience-updated-locations.csv");