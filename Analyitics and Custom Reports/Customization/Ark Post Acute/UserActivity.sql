USE [ARKPOSTACUTE]

SELECT DISTINCT
	[o].[OrganizationName] AS [Location],
	[j].[JobName] AS [Job Title],
	[applicantInfo].[Applicant Name] AS [Applicant Name],
	ISNULL([noteDates].[Comment Dates], '') AS [Application Note],
	ISNULL(appReport.[Application], '') AS [Viewed Application],
	ISNULL(resumes.[Resume], '') AS [Viewed Resume],
	ISNULL([pNotes].[createdNote], '') AS [Created Process Note]
FROM [ApplicationJobOrganization] [ajo]
INNER JOIN [Organization] [o] ON [ajo].[OrganizationID] = [o].[OrganizationID]
INNER JOIN [Job] [j] ON [ajo].[JobID] = [j].[JobID]

INNER JOIN (
	SELECT DISTINCT
		[a].[ApplicationID],
		([c].[FirstName] + ' ' + [c].[LastName]) AS [Applicant Name]
	FROM [Application] [a]
	INNER JOIN [Candidate] [c] ON [a].[ApplicationID] = [c].[CandidateId]

) AS [applicantInfo] ON [applicantInfo].[ApplicationID] = [ajo].[ApplicationID]

left JOIN (
	SELECT
		[an].[ApplicationJobOrganizationID],
		STRING_AGG(([u].[FirstName] + ' ' + [u].[LastName]), ', ') AS [Comment Dates]
	FROM [ApplicationNote] [an]
	INNER JOIN [User] [u] ON [an].[PersonID] = [u].[PersonID]
 	
	GROUP BY [an].[ApplicationJobOrganizationID]
) AS [noteDates] ON [noteDates].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID] 

left join (
	SELECT 
		[p].[ApplicationJobOrganizationID],
		ISNULL(STRING_AGG(([u].[FirstName] + ' ' + [u].[LastName]), ', '), '') AS [Application]
	FROM [Process] [p]
	INNER JOIN [ProcessLog] [pl] ON [p].[ProcessID] = [pl].[ProcessId]
	INNER JOIN [User] [u] ON [pl].[UserId] = [u].[UserID] AND [u].[FirstName] != 'HealthcareSource'
	where [p].[ProcessDefinitionID] = 'b1592e03-64b2-4581-9fd2-91f1dbe8b0f0' 
	AND [pl].[UserId] <> 'f133cf4c-389d-4089-ad16-5378c6458f91'
	AND [pl].StatusCode = 1
	GROUP BY [p].[ApplicationJobOrganizationID]
	
) AS [appReport] On [ajo].[ApplicationJobOrganizationID] = [appReport].ApplicationJobOrganizationID

left join (
	SELECT 
		[p].[ApplicationJobOrganizationID],
	
		ISNULL(STRING_AGG([userData].[name], ', '), '') AS [Resume]
	FROM [Process] [p]
	INNER JOIN (
		SELECT DISTINCT
			[pl].[ProcessId],
			([u].[FirstName] + ' ' + [u].[FirstName]) AS [name]
		FROM [ProcessLog] [pl]
		INNER JOIN [User] [u] ON [u].[UserID] = [pl].UserId
		WHERE [u].[FirstName] != 'HealthcareSource'
	) AS [userData] ON [userData].[ProcessId] = [p].[ProcessID]

	INNER JOIN [ProcessLog] [pl] ON [p].[ProcessID] = [pl].[ProcessId] AND [p].[ProcessDefinitionID] = '23b09ca4-3e23-46cb-af0a-805eb456374e'
	
	WHERE [pl].StatusCode = 1 
	GROUP BY [p].[ApplicationJobOrganizationID]
	
	
) AS [resumes] On [ajo].[ApplicationJobOrganizationID] = [resumes].[ApplicationJobOrganizationID]


left JOIN (
	SELECT DISTINCT
		[p].[ApplicationJobOrganizationID],
	
		ISNULL(STRING_AGG([userData].[name], ', '), '') AS [createdNote]
	FROM [Process] [p]
	INNER JOIN (
		SELECT DISTINCT
			[pn].[ProcessId],
			([u].[FirstName] + ' ' + [u].[FirstName]) AS [name]
		FROM [ProcessNote] [pn]
		INNER JOIN [User] [u] ON [pn].[PersonID] = [u].[PersonID]
	) AS [userData] ON [userData].[ProcessId] = [p].[ProcessID]


 	
	GROUP BY [p].[ApplicationJobOrganizationID]
) AS [pNotes] ON [pNotes].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID] 

WHERE [ajo].[AvailableDate] BETWEEN DATEADD(DAY, -6, GETDATE()) AND GETDATE()

