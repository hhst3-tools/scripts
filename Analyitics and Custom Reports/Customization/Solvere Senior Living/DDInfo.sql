SELECT
	[o].[OrganizationName],
	[j].[JobName],
	[c].[FirstName],
	[c].[LastName],
	[c].[SocialSecurityNumber],
	ISNULL([aXML].ResponseData.value('(/responses/AccountNumber1)[1]', 'VARCHAR(MAX)'), '') AS [AccountNumber1],
	ISNULL([aXML].ResponseData.value('(/responses/RoutingNumber1)[1]', 'VARCHAR(MAX)'), '') AS [RoutingNumber1],
	ISNULL([aXML].ResponseData.value('(/responses/AccountType1)[1]', 'VARCHAR(MAX)'), '') AS [AccountType1],
	CASE
		WHEN ISNULL([aXML].ResponseData.value('(/responses/DirectDepositIsPercent)[1]', 'BIT'), '') = 1 THEN 'Full Amount'
		WHEN ISNULL([aXML].ResponseData.value('(/responses/DirectDepositIsPercent)[1]', 'BIT'), '') = 0 THEN 'Partial Amount'
		ELSE '' 
	END AS [PartialOrFullAmount1],
	
	ISNULL([aXML].ResponseData.value('(/responses/DirectDepositIsPercent)[1]', 'BIT'), '') AS [DirectDepositIsPercent],
	ISNULL([aXML].ResponseData.value('(/responses/DirectDepositAmountOrPercent)[1]', 'VARCHAR(MAX)'), '') AS [DirectDepositAmountOrPercent],
	ISNULL([aXML].ResponseData.value('(/responses/AccountNumber2)[1]', 'VARCHAR(MAX)'), '') AS [AccountNumber2],
	ISNULL([aXML].ResponseData.value('(/responses/RoutingNumber2)[1]', 'VARCHAR(MAX)'), '') AS [RoutingNumber2],
	ISNULL([aXML].ResponseData.value('(/responses/AccountType2)[1]', 'VARCHAR(MAX)'), '') AS [AccountType2],
		CASE
		WHEN ISNULL([aXML].ResponseData.value('(/responses/DirectDepositIsPercent2)[1]', 'BIT'), '') = 1 THEN 'Full Amount'
		WHEN ISNULL([aXML].ResponseData.value('(/responses/DirectDepositIsPercent2)[1]', 'BIT'), '') = 0 THEN 'Partial Amount'
		ELSE '' 
	END AS [PartialOrFullAmount2],
	ISNULL([aXML].ResponseData.value('(/responses/DirectDepositAmountOrPercent2)[1]', 'VARCHAR(MAX)'), '') AS [DirectDepositIsPercent2]


FROM [ApplicationJobOrganization] [ajo]
INNER JOIN [Organization] [o] ON [o].[OrganizationID] = [ajo].[OrganizationID]
INNER JOIN [Job] [j] ON [j].[JobID] = [ajo].[JobID]
INNER JOIN [Offer] [off] ON [off].[ApplicationJobOrganizationId] = [ajo].[ApplicationJobOrganizationID] AND [off].[StartDate] >= '2020-12-01' 
INNER JOIN [Application] [a] ON [a].[ApplicationID]  = [ajo].[ApplicationID]
INNER JOIN [Candidate] [c] ON [c].[CandidateId] = [ajo].[ApplicationID]
INNER JOIN [ApplicationXmlData] [aXML] ON [aXML].[ApplicationID] = [a].[ApplicationID]

WHERE ([o].[OrganizationName] = 'Pavilion at Great Hills'
	OR [o].[OrganizationName] = 'The Monarch at Richardson'
	OR [o].[OrganizationName]= 'Park Place at Winghaven'
	OR [o].[OrganizationName] = 'The Hamptons at Meadows Place')
