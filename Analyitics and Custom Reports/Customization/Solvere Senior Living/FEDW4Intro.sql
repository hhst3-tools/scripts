SELECT
	[o].[OrganizationName],
	[j].[JobName],
	[c].[FirstName],
	[c].[LastName],
	[c].[SocialSecurityNumber],
	ISNULL([aXML].ResponseData.value('(/responses/FederalW4ClaimStatus)[1]', 'VARCHAR(MAX)'), '') AS [MaritalStatus],
	ISNULL([aXML].ResponseData.value('(/responses/FederalW4MultipleJobs)[1]', 'VARCHAR(MAX)'), '') AS [MultipleJobs],
	ISNULL([aXML].ResponseData.value('(/responses/FederalW4ExemptionsClaimed)[1]', 'VARCHAR(MAX)'), '') AS [ExemptionsClaimed],
	ISNULL([aXML].ResponseData.value('(/responses/FederalW4OtherJobs)[1]', 'VARCHAR(MAX)'), '') AS [OtherJobs],
	ISNULL([aXML].ResponseData.value('(/responses/FederalW4NumberOfChildren)[1]', 'VARCHAR(MAX)'), '') AS [ClaimedDependents],
	ISNULL([aXML].ResponseData.value('(/responses/FederalW4OtherDependents)[1]', 'VARCHAR(MAX)'), '') AS [OtherDependents],
	ISNULL([aXML].ResponseData.value('(/responses/FederalW4ExtraWithhold)[1]', 'VARCHAR(MAX)'), '') AS [ExtraWitholding],
	ISNULL([aXML].ResponseData.value('(/responses/FederalW4OtherDeductions)[1]', 'VARCHAR(MAX)'), '') AS [OtherDeductions],
	ISNULL([aXML].ResponseData.value('(/responses/FederalW4IsExempt)[1]', 'VARCHAR(MAX)'), '') AS [IsExempt]


FROM [ApplicationJobOrganization] [ajo]
INNER JOIN [Organization] [o] ON [o].[OrganizationID] = [ajo].[OrganizationID]
INNER JOIN [Job] [j] ON [j].[JobID] = [ajo].[JobID]
INNER JOIN [Offer] [off] ON [off].[ApplicationJobOrganizationId] = [ajo].[ApplicationJobOrganizationID] AND [off].[StartDate] >= '2020-12-01' 
INNER JOIN [Application] [a] ON [a].[ApplicationID]  = [ajo].[ApplicationID]
INNER JOIN [Candidate] [c] ON [c].[CandidateId] = [ajo].[ApplicationID]
INNER JOIN [ApplicationXmlData] [aXML] ON [aXML].[ApplicationID] = [a].[ApplicationID]

WHERE ([o].[OrganizationName] = 'Pavilion at Great Hills'
	OR [o].[OrganizationName] = 'The Monarch at Richardson'
	OR [o].[OrganizationName]= 'Park Place at Winghaven'
	OR [o].[OrganizationName] = 'The Hamptons at Meadows Place')
