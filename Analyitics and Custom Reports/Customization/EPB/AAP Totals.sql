USE [EPB]

SELECT DISTINCT
	SUM(ISNULL([Total].[Count], 0)) AS [Total # of Applicants],
	SUM(ISNULL([TotalCompleted].[Count], 0)) AS [Submitted Total],
	CAST(SUM(ISNULL([TotalFemale].[Count], 0)) AS varchar)  + '/' + cast(SUM(ISNULL([TotalMale].[Count], 0)) as varchar) AS [Submitted W/M],
	SUM(ISNULL([TotalInterview].[Count], 0)) AS [Interview Total],
	CAST(SUM(ISNULL([TotalInterviewFemale].[Count], 0)) AS varchar) + '/' + CAST(SUM(ISNULL([TotalInterviewMale].[Count], 0)) AS varchar) AS [Interview W/M],
	SUM(ISNULL([offers].[Count], 0)) AS [Total Offers],
	CAST(SUM(ISNULL([offersFemale].[Count], 0)) as varchar) + '/' + CAST(SUM(ISNULL([offersMale].[Count], 0)) as varchar)  AS [Total Offers W/M]
FROM [ApplicationJobOrganization] [ajo]

OUTER APPLY (
	SELECT DISTINCT
		[aXML].[ApplicationID],
		ISNULL([aXML].ResponseData.value('(/responses/SelfIDGender)[1]', 'varchar'), '') AS [Gender]
	FROM [ApplicationXmlData] [aXML]
	WHERE [aXML].[ApplicationID] = [ajo].[ApplicationID]
) AS [app] 

OUTER APPLY (
   SELECT
	  COUNT([ajo1].ApplicationJobOrganizationID) AS [Count]
	FROM [ApplicationJobOrganization] [ajo1]
	WHERE [ajo1].[AvailableDate] IS NOT NULL
		AND [ajo1].[ApplicationJobOrganizationID] = [ajo].ApplicationJobOrganizationID
) AS [Total]  

OUTER APPLY (
   SELECT
	  COUNT([ajo2].[ApplicationJobOrganizationID]) AS [Count]
	FROM [ApplicationJobOrganization] [ajo2]
	WHERE [ajo2].[ApplicationDate] IS NOT NULL
		AND [ajo2].ApplicationJobOrganizationID = [ajo].[ApplicationJobOrganizationID]
) AS [TotalCompleted] 

OUTER APPLY (
	SELECT
		COUNT([ajo3].[ApplicationJobOrganizationID]) AS [Count]
	FROM [ApplicationJobOrganization] [ajo3]
	WHERE [ajo3].[ApplicationDate] IS NOT NULL
		AND [ajo3].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID]
		AND [app].[Gender] = 'M'
) AS [TotalMale]

OUTER APPLY (
	SELECT
		COUNT([ajo4].[ApplicationJobOrganizationID]) AS [Count]
	FROM [ApplicationJobOrganization] [ajo4]
	WHERE [ajo4].[ApplicationDate] IS NOT NULL
		AND [ajo4].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID]
		AND [app].[Gender] = 'F'
) AS [TotalFemale]

-- TOTAL INTERVIEWS
OUTER APPLY (
	SELECT
		COUNT([p].[ApplicationJobOrganizationID]) AS [Count]
	FROM [ProcessDefinition] [pd]
		INNER JOIN [Process] [p] ON [p].[ProcessDefinitionID] = [pd].[ProcessDefinitionID]
	WHERE [pd].[Name] = 'Panel Interview Questionnaire' 
		AND ([p].[ScoreValue] = 2 OR [p].[ScoreValue] > 20)
		AND [p].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID]
) AS [TotalInterview] 

-- FEMALE INTERVIEWS
OUTER APPLY (
	SELECT
		COUNT([p].[ApplicationJobOrganizationID]) AS [Count]
	FROM [ProcessDefinition] [pd]
		INNER JOIN [Process] [p] ON [p].[ProcessDefinitionID] = [pd].[ProcessDefinitionID]
	WHERE [pd].[Name] = 'Panel Interview Questionnaire' 
		AND ([p].[ScoreValue] = 2 OR [p].[ScoreValue] > 20)
		AND [p].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID] 
		AND [ajo].[ApplicationDate] IS NOT NULL
		AND [app].[Gender] = 'F'
) AS [TotalInterviewFemale]

-- MALE INTERVIEWS
OUTER APPLY (
	SELECT
		COUNT([p].[ApplicationJobOrganizationID]) AS [Count]
	FROM [ProcessDefinition] [pd]
		INNER JOIN [Process] [p] ON [p].[ProcessDefinitionID] = [pd].[ProcessDefinitionID]
	WHERE [pd].[Name] = 'Panel Interview Questionnaire' 
		AND ([p].[ScoreValue] = 2 OR [p].[ScoreValue] > 20)
		AND [p].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID] 
		AND [ajo].[ApplicationDate] IS NOT NULL
		AND [app].[Gender] = 'M'
) AS [TotalInterviewMale]

-- Offer Total 
OUTER APPLY (
	SELECT
		COUNT([o].ApplicationJobOrganizationId) AS [Count]
	FROM [Offer] [o]
	WHERE [o].[ApplicationJobOrganizationId] = [ajo].[ApplicationJobOrganizationID]
) AS [offers]

-- Offer to Males Total
OUTER APPLY (
	SELECT
		COUNT([o].ApplicationJobOrganizationId) AS [Count]
	FROM [Offer] [o]
	WHERE [o].[ApplicationJobOrganizationId] = [ajo].[ApplicationJobOrganizationID]
	AND [app].[Gender] = 'M'
) AS [offersMale]

-- Offer to Female Total
OUTER APPLY (
	SELECT
		COUNT([o].ApplicationJobOrganizationId) AS [Count]
	FROM [Offer] [o]
	WHERE [o].[ApplicationJobOrganizationId] = [ajo].[ApplicationJobOrganizationID]
	AND [app].[Gender] = 'F'
) AS [offersFemale]

WHERE [ajo].[AvailableDate] IS NOT NULL
	AND [ajo].[AvailableDate] BETWEEN DATEADD(WEEK, -1, GETDATE()) AND GETDATE()
	AND [ajo].[IsActive] = 1