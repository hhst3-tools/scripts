USE [EPB]

SELECT DISTINCT
	ISNULL((CAST([lr].[RequestYear] AS varchar) + '-' + RIGHT('0000' + CAST([lr].[RequestNumber] AS varchar),5)), '') AS FullRequestId,
	CASE
		WHEN [applicant].[IsInternal] = 1 THEN 'Y'
		WHEN [applicant].[IsInternal] = 0 THEN 'N'
	ELSE '' END AS 'Internal',
	([applicant].[FirstName] + ' ' + [applicant].[LastName]) AS 'Candidate_Name',
	[applicant].[RaceCode],
	-- Gender
	ISNULL(FORMAT([ajo].ApplicationDate, 'MM/dd/yyyy'), '') AS 'Application_Signed_Date',
	[j].[JobName] AS 'Job_Title',
	-- HMR
	-- TESTING (y/n)
	-- Interview (y/n)
	[interviews].[StatusCode],

	CASE
		
		WHEN [ajo].[ApplicationStatus] = 7 THEN 'NH'
		WHEN [ajo].[ApplicationStatus] = 8 THEN 'H'
	ELSE '' END AS 'Action Taken',
	ISNULL(FORMAT([off].[StartDate], 'MM/dd/yyyy'), '') AS 'Hire_Date',
	[jt].[Name] AS [Job Group]

FROM [OrganizationJob] [oj]
INNER JOIN [Organization] [o] ON [oj].[OrganizationID] = [o].[OrganizationID]
INNER JOIN [Job] [j] ON [j].[JobID] = [oj].[JobID]
INNER JOIN [JobType] [jt] ON [jt].[JobTypeID] = [j].[JobTypeID]
INNER JOIN [LaborRequest] [lr] ON [lr].OrganizationJobId = [oj].[OrganizationJobID]

INNER JOIN [ApplicationJobOrganization] [ajo] ON [ajo].[JobID] = [oj].[JobId] and [ajo].[OrganizationID] = [oj].[OrganizationID] AND [ajo].ApplicationDate IS NOT NULL

INNER JOIN (
	SELECT
		[p].[ApplicationJobOrganizationID],
		[p].[StatusCode]
	FROM [Process] [p]
	WHERE [p].[ProcessDefinitionID] = 'e05cf9c6-781e-4208-b0c9-003b270373f1'
) As interviews ON [interviews].[ApplicationJobOrganizationId] = [ajo].[ApplicationJobOrganizationId]


INNER JOIN (
	SELECT DISTINCT
	 [c].[CandidateId],
	 [c].[FirstName],
	 [c].[LastName],
	 [c].[IsInternal],
	 [a].[RaceCode]
	FROM [Candidate] [c]
	INNER JOIN [Application] [a] ON [a].[ApplicationID] = [c].[CandidateId]
	
) AS [applicant] ON [applicant].[CandidateId] = [ajo].[ApplicationID]




LEFT JOIN [Offer] [off] ON [off].[ApplicationJobOrganizationId] = [ajo].[ApplicationJobOrganizationID]


WHERE  [ajo].[AvailableDate] BETWEEN DATEADD(month, -1, GETDATE()) AND GETDATE()
