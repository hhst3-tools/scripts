USE [EPB]

-- Pulls individual applicants. Some applicants applied to positions with no open requeistions
-- Race or Gender might be null. These are only required during onboarding if an offer is made

SELECT
	ISNULL([req].[RequestId], 0) AS [Requistion],
	[app].[Internal] AS [Internal/External],
	[app].[Name],
	ISNULL([app].[RaceCode], ' ') AS [Race/Ethnicity],
	[app].[Gender],
	FORMAT([ajo].[AvailableDate], 'MM-dd-yyyy') AS [Date Of Application],
	[j].[JobName] AS [Job Title],
	-- HRM - Create Blank Column in SSRS
	-- Testing Create Blank Column in SSRS
	ISNULL([Interview].[interviewStatus], 'N') AS [Interview (Y/N)],
	CASE 
		WHEN [off].[StartDate] IS NULL THEN 'NH'
		WHEN [off].[StartDate] IS NOT NULL THEN 'H'
	ELSE '' END AS [Action Taken (H/NH)],
	ISNULL(FORMAT([off].[StartDate], 'MM-dd-yyyy'), '') AS [Hire Date],
	[jt].[Name] AS [Job Group]
FROM [OrganizationJob] [oj]
INNER JOIN [Job] [j] ON [j].[JobID] = [oj].[JobID]
LEFT JOIN [JobType] [jt] ON [jt].[JobTypeID] = [j].[JobTypeID]
INNER JOIN [ApplicationJobOrganization] [ajo] on [ajo].[OrganizationID] = [oj].[OrganizationID] and [ajo].[JobID] = [oj].[JobID]


INNER JOIN (
	SELECT
		[a].[ApplicationID],
		[c].[FirstName] + ' ' + [c].[LastName] AS [Name],
		CASE
			WHEN [c].[isInternal] = 1 THEN 'Internal'
			WHEN [c].[isInternal] = 0 THEN 'External'
		ELSE '' END AS [Internal],
		ISNULL([a].[RaceCode], ' ') as [RaceCode],
		ISNULL([aXML].ResponseData.value('(/responses/SelfIDGender)[1]', 'varchar'), '') AS [Gender]
	FROM [Application] [a]
	INNER JOIN [ApplicationXmlData] [aXML] ON [aXML].[ApplicationID] = [a].[ApplicationID]
	LEFT JOIN [Candidate] [c] ON [c].[CandidateId] = [a].[ApplicationID]
	
) as [app] ON [app].[ApplicationID] = [ajo].[ApplicationID]

LEFT JOIN [Offer] [off] ON [off].[ApplicationJobOrganizationId] = [ajo].[ApplicationJobOrganizationID]

OUTER APPLY (
	SELECT
	CASE
		 WHEN [p].[ScoreValue] > 0 THEN 'Y'
		 WHEN [p].[ScoreValue] IS NULL  THEN 'N'
	ELSE '' END AS [interviewStatus]
	FROM [ProcessDefinition] [pd]
	INNER JOIN [Process] [p] ON [p].[ProcessDefinitionID] = [pd].[ProcessDefinitionID]
	WHERE [p].[ApplicationJobOrganizationID] = [ajo].[ApplicationJobOrganizationID]
	AND [pd].[Name] = 'Panel Interview Questionnaire' 
) AS interview

OUTER APPLY (
	SELECT
		ISNULL((CAST([lr].[RequestYear] AS varchar) + '-' + RIGHT('0000' + CAST([lr].[RequestNumber] AS varchar),5)), '') AS RequestId
	FROM LaborRequest [lr]
	
	WHERE [lr].[ApplicationJobOrganizationId] = [ajo].[ApplicationJobOrganizationID]

) AS req


WHERE [ajo].[AvailableDate] IS NOT NULL
	  AND [ajo].[AvailableDate] BETWEEN DATEADD(WEEK, -1, GETDATE()) AND GETDATE()
	  AND [ajo].[IsActive] = 1