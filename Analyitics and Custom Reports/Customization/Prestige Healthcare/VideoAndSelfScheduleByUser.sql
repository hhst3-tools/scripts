USE [PRESTIGEHEALTHCARE]

SELECT DISTINCT
	[o].[OrganizationName],
	ISNULL([c].[FirstName] + ' ' + [c].[LastName], '') AS [Applicant],
	ISNULL([vid].[User], '') AS [Video User],
	ISNULL(FORMAT([vid].[StartDate], 'MM-dd-yyyy'), '') AS [Video Meeting Date],
	ISNULL([sch].[Self-Scheduled Date], '') AS [Self-Scheduled Date],
	ISNULL([sch].[User], '') AS [Self-Scheduled User]
FROM ApplicationJobOrganization ajo
INNER JOIN Candidate c ON ajo.ApplicationID = c.CandidateId
INNER JOIN Organization o ON ajo.OrganizationID = o.OrganizationID

LEFT JOIN (
	SELECT
		vmr.ApplicationJobOrganizationId,
		[u].[FirstName] + ' ' + [u].[LastName] AS [User],
		[vm].[StartDate]
		
	FROM VideoMeetingRoom vmr
	INNER JOIN VideoMeeting vm ON vmr.Id = vm.VideoMeetingRoomId
	INNER JOIN  VideoAttendee va ON va.MeetingId = vm.MeetingId
	INNER JOIN [User] [u] ON va.UserId = u.UserID
) AS vid ON vid.ApplicationJobOrganizationId = ajo.ApplicationJobOrganizationID

LEFT JOIN ( 
	SELECT
		[p].[ApplicationJobOrganizationID],
		[u].[FirstName] + ' ' + [u].[LastName] AS [User],
		FORMAT([m].[CreatedDate], 'MM-dd-yyyy') AS [Self-Scheduled Date]
	FROM [Meeting] [m]
	INNER JOIN [Process] [p] ON [p].ProcessID = [m].[ProcessId]
	INNER JOIN [User] [u] ON [m].[SendingUserId] = [u].[UserID]
)AS sch ON [sch].[ApplicationJobOrganizationID] = [ajo].ApplicationJobOrganizationID

WHERE ajo.AvailableDate BETWEEN DATEADD(Month, -1, GETDATE()) AND GETDATE()