USE [PRESTIGEHEALTHCARE]

SELECT 
	o.OrganizationName,
	ISNULL([video].[Location Count], 0) as [Total Video Confrences],
	ISNULL([sch].[Location Count], 0) as [Total Self-Scheduled]

FROM Organization o

LEFT JOIN (
	SELECT 
		ajo.OrganizationID,
		count(1) AS [Location Count]
	FROM VideoMeetingRoom vmr
	INNER JOIN ApplicationJobOrganization ajo ON vmr.ApplicationJobOrganizationId = ajo.ApplicationJobOrganizationID
	WHERE ajo.AvailableDate BETWEEN DATEADD(Month, -1, GETDATE()) AND GETDATE()
	GROUP BY ajo.OrganizationID
) AS video ON o.OrganizationID = o.OrganizationID

LEFT JOIN ( 
	SELECT 
		ajo.OrganizationID,
		COUNT(1) AS [Location Count]
	FROM Meeting m
	INNER JOIN Process p ON m.ProcessId = p.ProcessID
	INNER JOIN ApplicationJobOrganization ajo ON p.ApplicationJobOrganizationID = ajo.ApplicationJobOrganizationID
	WHERE ajo.AvailableDate BETWEEN DATEADD(Month, -1, GETDATE()) AND GETDATE()
	

	group by ajo.OrganizationID
)AS sch ON sch.OrganizationID = o.OrganizationID

GROUP BY o.OrganizationName, [video].[Location Count], [sch].[Location Count]