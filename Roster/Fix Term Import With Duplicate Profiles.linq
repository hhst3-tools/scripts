<Query Kind="Statements">
  <Connection>
    <ID>234a9303-3ad4-4d60-962d-63afc17a7a23</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Test\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>

var ctx = Connection.GetSystemUserToken(ClientIds.MidwestHealthAccount).GetCoreContext();

var hireLog = ctx.Employee.Find(new Guid("e534fa97-4f3b-46b1-ae44-3aca7bd282f6")).EmployeeLog.First();
var newLog = new Vikus.Data.Client.EmployeeLog() {
	EmployeeId = new Guid("f9ed2071-8080-483b-972b-ef4ceb607374"),
	ChangeDate = new DateTime(2020, 05, 17, 05, 44, 37),
	OrganizationId = new Guid("42219bc7-0e15-4504-8da7-189b9df024d4"),
	JobId = new Guid("55931e50-5dbf-4dcd-b379-5a06596e541e")
	Status = hireLog.Status();
}
//var termedEmployee = ctx.Employee.Find(new Guid("f9ed2071-8080-483b-972b-ef4ceb607374")).EmployeeLog.Add(new Vikus.Data.Client.EmployeeLog {
//	
//});

