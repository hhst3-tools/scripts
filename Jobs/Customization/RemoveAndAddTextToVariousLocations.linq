<Query Kind="Statements">
  <Connection>
    <ID>234a9303-3ad4-4d60-962d-63afc17a7a23</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Test\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>

var ctx = Connection.GetSystemUserToken(ClientIds.SignatureHealthcareAccount).GetCoreContext();

/*
Do Not Parse these locations
HomeNow, 
SHC Medical Partners(Integritas),
Signature Lifestyles
Silver Angels
9067 - YourDoc2U
9901 - Signature Advantage 
*/

var removeText = new List<string> {
	@"Signature HealthCARE (SHC) is an industry leader in post-acute, long term care and rehabilitation services. We operate over 110 locations across 10 states employing over 17,000 Stakeholders (our thoughtful and loving employees). Our Vision is to lead radical change across the healthcare landscape to transform lives. Our Mission is to revolutionize the healthcare experience for those we serve, through an innovative culture of person directed care, individualized spirituality and meaningful quality of life driven by stakeholder learning and purpose.",
	@"Our Sacred Six Values of Compassion, Teamwork, Respect, Integrity, Patience and Positivity make this an incredible place to serve and make a difference in the lives of our patients, residents, Stakeholders and communities!",
	@"A growing number of SHC centers are earning Five-Star ratings from the Centers for Medicare and Medicaid Services. We were also named one of Modern Healthcare’s “Best Places to Work” three times! Our culture is unparalleled and founded on three pillars: Learning, Spirituality, and Innovation. Recently, we achieved number #3 ranking under Large Companies for Best Workplaces for Aging Services in recent news! Come see what the Revolution is all about!",
	@"Signature HealthCARE strives to be pioneers in total rewards to ensure that we can attract the very best talent. We offer all of the expected health and wellness benefits,but we are proud to go beyond the usual including FREE single coverage medical! Please inquire about the details so we can share what more is offered in your market.",
	@"A growing number of SHC centers are earning Five-Star ratings from the Centers for Medicare and Medicaid Services. We were also named one of Modern Healthcare’s “Best Places to Work” three times! Our culture is unparalleled and founded on three pillars: Learning, Spirituality, and Innovation. Recently, we achieved number #3 ranking under Large Companies for Best Workplaces for Aging Services in recent news! Come see what the Revolution is all about!",
	@"Are you looking for a little more creativity, challenge, and growth opportunity in your workday? Didn’t think it was possible? Might be time to reconsider.",
	@"At Signature HealthCARE, our team members are permitted – no, encouraged – to employ their talents and abilities to solve problems. Our culture is built on three distinct pillars: Learning, Spirituality and Intra-preneurship. But this isn’t just hollow corporate sloganeering. Each pillar has its own staff and initiatives, ensuring that our unique culture permeates the entire organization.",
	@"Oh, by the way, we’re an elder care company. Our mission? To radically change the landscape of long-term care forever.",
	@"Signature HealthCARE has a vision to radically change the landscape of healthcare forever. It’s more than a corporation… it’s a Revolution. We are currently seeking an innovative and progressive leader to join the mission.",
	@"Signature HealthCARE is an industry leader in post-acute, long term care and rehabilitation services. We operate more than 115 locations across 10 states employing over 17,000 Stakeholders (our thoughtful and loving employees)",
	@"A growing number of Signature centers are earning Five-Star ratings from the Centers for Medicare & Medicaid Services. We were also named one of Modern Healthcare’s “Best Places to Work” three times! Signature’s culture is unparalleled and founded on three pillars: Learning, Spirituality, and Innovation. Come see what the Revolution is all about!"
};

var locationAdText = "<br><p style=\"color: red;\">Signature HealthCARE is a family-based healthcare company that offers integrated services in 10 states across the continuum of care: skilled nursing, rehabilitation, assisted living, memory care, home health, cognitive care, and telemedicine.</p><br><p style=\"color: red;\">A growing number of our centers are earning quality assurance accreditation and pioneering person - directed care.Many of our skilled nursing facilities have achieved a 4 or 5 - star overall rating from the Centers for Medicare & Medicaid Services.Additionally, we have been awarded as a certified Great Place to Work for three years in a row and Modern Healthcare’s “Best Places to Work”!</p>";


var orgNamefilters = new List<string> { "HomeNow", "SHC Medical Partners", "Signature Lifestyles", "Silver Angels", "9067", "9901", };
var externalOrgNameFilters = new List<string> {"Rehabilitation", "Rehab"};
var orgs = ctx.Organization;

//Filtering out jobs
var excludedOrgs = new List<Guid> { };
orgNamefilters.Map(n => orgs.Where(o => o.OrganizationName.Contains(n))
                            .Map(o => excludedOrgs.Add(o.OrganizationId)));

externalOrgNameFilters.Map(n => orgs.Where(o => o.ExternalName.Contains(n))
			  		                .Map(o => excludedOrgs.Add(o.OrganizationId)));


var filteredOrgs = orgs.Where(o => !o.OrganizationChildrenAncestors.Any(o => excludedOrgs.Contains(o.AncestorId)));

//Removing Job Add Text
removeText.Map(t => ctx.OrganizationJob.Where(oj => filteredOrgs.Contains(oj.Organization))
									   .Where(oj => oj.Specifics.Contains(t))
								       .Pipe(oj => oj.Specifics = oj.Specifics.Trim().Replace(t, "")));

//Updating Location Ads
filteredOrgs.Where(o => o.LocationJobDescription == null).Pipe(oj => oj.LocationJobDescription = locationAdText );
filteredOrgs.Where(o => !o.LocationJobDescription.Contains(locationAdText)).Pipe(o => o.LocationJobDescription = o.LocationJobDescription.Trim() + locationAdText  );
ctx.SaveChanges();
