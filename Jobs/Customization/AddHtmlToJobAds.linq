<Query Kind="Statements">
  <Connection>
    <ID>234a9303-3ad4-4d60-962d-63afc17a7a23</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Test\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>

var ctx = Connection.GetSystemUserToken(ClientIds.CapitalHealthServicesAccount).GetCoreContext();


var htmlToAdd = "<div><br><p>At Capital Health Care Network, our company culture is built on the following seven essential skills of teamwork:</p><ul style=\"list-style: none;\"><li><strong> Active Listening: </strong> Team members listen to each other’s ideas.They are observed validating ideas through active listening and “piggy-backing” (or building) off each other’s ideas.</li> <li><strong>Communication: </strong> Team members communicate.They are observed interacting, discussing, and posing questions in an effort to fortify understanding and dispel miscommunication. </li> <li><strong>Persuading: </strong> Team members use persuasion. They are observed exchanging, defending, and rethinking ideas with the greater good in mind.</li> <li><strong>Respecting: </strong> Team members respect the opinions of others.They are observed encouraging and supporting others’ ideas and efforts.</li> <li><strong>Helping: </strong> Team members help.They are observed offering assistance to each other. </li> <li><strong>Sharing: </strong> Team members share.They are observed sharing ideas, information and influence.</li><li><strong>Participating: </strong> Team members participate.They are observed participating in social media, campaigns, and projects.</li></ul></div>";
var texTileToAdd = @"p. At Capital Health Care Network, our company culture is built on the following seven essential skills of teamwork:

p. *Active Listening:* Team members listen to each other’s ideas. They are observed validating ideas through active listening and “piggy-backing” (or building) off each other’s ideas.
p. *Communication:* Team members communicate. They are observed interacting, discussing, and posing questions in an effort to fortify understanding and dispel miscommunication.
p. *Persuading:* Team members use persuasion. They are observed exchanging, defending, and rethinking ideas with the greater good in mind.
p. *Respecting:* Team members respect the opinions of others. They are observed encouraging and supporting others’ ideas and efforts.
p. *Helping:* Team members help. They are observed offering assistance to each other.
p. *Sharing:* Team members share. They are observed sharing ideas, information and influence.
p. *Participating:* Team members participate. They are observed participating in social media, campaigns, and projects.
";

ctx.OrganizationJob.Where(oj => oj.DescriptionSequence != Vikus.Data.Client.DescriptionSequence.LocationOpeningJob).Pipe(oj => oj.DescriptionSequence = Vikus.Data.Client.DescriptionSequence.LocationOpeningJob);
ctx.SaveChanges();

var jobs = ctx.Job.AsEnumerable();
jobs.Where(j => j.QuickDescription.Trim().StartsWith("<") && !j.QuickDescription.Contains(htmlToAdd)).Pipe(j => j.QuickDescription = j.QuickDescription.Trim() + htmlToAdd);
jobs.Where(j => !j.QuickDescription.Trim().StartsWith("<") && !j.QuickDescription.Contains(texTileToAdd)).Pipe(j => j.QuickDescription = j.QuickDescription.Trim() + $" {texTileToAdd}");
ctx.SaveChanges();
