<Query Kind="Statements">
  <Connection>
    <ID>234a9303-3ad4-4d60-962d-63afc17a7a23</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Test\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
  <IncludeLinqToSql>true</IncludeLinqToSql>
  <IncludeAspNet>true</IncludeAspNet>
  <RuntimeVersion>3.1</RuntimeVersion>
</Query>

var ctx = Connection.GetSystemUserToken(ClientIds.HeritageSeniorLivingAccount).GetCoreContext();

//

//Clear all location and job ads for clients
ctx.OrganizationJob.Select(oj => oj ).Pipe(oj => oj.Specifics = "");
ctx.Organization.Select(o => o ).Pipe(o => o.LocationJobDescription = "");
ctx.SaveChanges();