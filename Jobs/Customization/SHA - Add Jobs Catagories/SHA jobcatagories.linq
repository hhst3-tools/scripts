<Query Kind="Program">
  <Connection>
    <ID>234a9303-3ad4-4d60-962d-63afc17a7a23</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Test\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
  <Namespace>Vikus.Data.Client</Namespace>
  <IncludeLinqToSql>true</IncludeLinqToSql>
  <IncludeAspNet>true</IncludeAspNet>
  <RuntimeVersion>3.1</RuntimeVersion>
</Query>


void Main() {
	var ctx = Connection.GetSystemUserToken(ClientIds.SignatureHealthcareAccount).GetCoreContext();
	var csvData = parseCsv(@"C:\SHA jobcatagories.csv");
	var allJobCatagories = ctx.JobCategory.ToList();
	var allJobs = ctx.Job.Where(j => !j.IsDeprecated ).ToList();
	var jobData = getUpdatedJobs(csvData, allJobs, allJobCatagories);
	
	allJobs.Where(j => jobData.Any(jd => jd.JobId == j.JobId) ).Pipe(j => j.JobCategoryId = jobData.Where(jd => jd.JobId == j.JobId).First().JobCategoryId);
	ctx.SaveChanges();
}

List<Vikus.Data.Client.Job> getUpdatedJobs(List<string[]> jobData, List<Vikus.Data.Client.Job> allJobs, List<Vikus.Data.Client.JobCategory> allCatagories) {
	var updatedJobs = new List<Vikus.Data.Client.Job>();
	
	jobData.Map(d => {
		var jobName = d[0];
		var catagoryName = d[1];
		var job = getJob(d[0], allJobs);
		var catagoryId = getCatagoryId(d[1], allCatagories);
		
		if ( job != null && catagoryId != -1) {
			job.JobCategoryId = catagoryId;
			updatedJobs.Add(job);
		}
	});
	
	return updatedJobs;
}

public static int getCatagoryId(string name, List<JobCategory> allCatagories) {
	if (!String.IsNullOrEmpty(name)) {
		var catagory = allCatagories.Where(jc => jc.Name == name).FirstOrDefault();
		if(catagory != null) {
			var catId = catagory.JobCategoryId;
			if(catId != -1) {return  catId;}
		}
		return -1;
	}
	
	return -1;
}

Vikus.Data.Client.Job getJob(string jobName, List<Job> allJobs) {
	return allJobs.Where(j => String.Equals(jobName, j.JobName)).FirstOrDefault();
}

List<string[]> parseCsv(string path) {
	var csv = File.ReadAllLines(path);
	var csvRow = new List<string[]> { };

	for (int i = 0; i < csv.Length; i++) {
		var row = csv[i].Split(",");
		var jobName = row[0].Replace("\"", "").Trim();
		var jobCode = row[2].Trim();

		if (!String.IsNullOrEmpty(jobName) && !String.IsNullOrEmpty(jobCode)) {

			string[] jobData = {row[0].Replace("\"", "").Trim(),
								row[2].Trim()};
			csvRow.Add(jobData);
		}
	}

	return csvRow;
}