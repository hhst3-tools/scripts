<Query Kind="Statements">
  <Connection>
    <ID>234a9303-3ad4-4d60-962d-63afc17a7a23</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\lib\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Test\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>

var ctx = Connection.GetSystemUserToken(ClientIds.SignatureHealthcareAccount).GetCoreContext();

var excludeJobs = new List<string> {
"Corp - Director of Compliance - Rehab",
"Corp - Rehab Chief Financial Officer",
"Corp - Rehab Financial Analyst",
"Corp - Rehab Help Desk Analyst",
"Corp - Rehab IS Analyst/Project Manager",
"Corp - Rehab Operations Officer (ROO)",
"Corp - Rehab Vice President of Business Development",
"Corp - Rehab Vice President of Clinical Services",
"Corp - Rehab Vice President of Operations",
"Corp - Rehab Vice President of Strategic Operations",
"Corp - Special Projects - Rehab Services Manager",
"Director of Medical Review - Rehab",
"Director of Rehab",
"HomeNow - Director of Rehab",
"President - Rehab",
"Rehab - Director of Clinical Services (DOC)",
"Rehab - Occupational Therapist",
"Rehab - Occupational Therapist Assistant",
"Rehab - Physical Therapist",
"Rehab - Physical Therapist Assistant",
"Rehab - Regional Operations Officer",
"Rehab - Rehab Services Manager",
"Rehab - Respiratory Therapist",
"Rehab - Restorative Aide",
"Rehab - Restorative Nurse Manager",
"Rehab - Speech Language Pathologist",
"Rehab - Therapist Assistant",
"Rehab Financial & IS Analyst",
"Rehab IS Analyst",
"Rehab Operations & Clinical Specialist",
"Rehab SVC Manager - COTA",
"Rehab SVC Manager - LPTA",
"Rehab SVC Manager - OT",
"Rehab SVC Manager - PT",
"Rehab SVC Manager - ST",
"Sample Job - Rehab / Therapy",
"Senior Rehab HR Generalist",
"Vice President of HR Rehab"};

//Remove from Job Add, Make sure data is only in location ad
var para1 = "Signature HealthCARE is a family-based healthcare company that offers integrated services in 10 states across the continuum of care: skilled nursing, rehabilitation, assisted living, memory care, home health, cognitive care, and telemedicine.";
var para2 = "A growing number of our centers are earning quality assurance accreditation and pioneering person - directed care. Many of our skilled nursing facilities have achieved a 4 or 5-star overall rating from the Centers for Medicare & Medicaid Services. Additionally, we have been awarded as a certified Great Place to Work for three years in a row and Modern Healthcare’s \"Best Places to Work\"!";
var jobsAdsWithP1 = ctx.Job.Where(j => j.QuickDescription.Contains(para1)).Count().Dump();
var jobsAdsWithP2 = ctx.Job.Where(j => j.QuickDescription.Contains(para2)).Count().Dump();



