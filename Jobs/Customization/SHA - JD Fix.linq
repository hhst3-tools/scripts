<Query Kind="Statements">
  <Connection>
    <ID>234a9303-3ad4-4d60-962d-63afc17a7a23</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Test\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>

var ctx = Connection.GetSystemUserToken(ClientIds.SignatureHealthcareAccount).GetCoreContext();

var excludeJobs = new List<string> {"Corp - Director of Compliance - Rehab",
"Corp - Rehab Chief Financial Officer",
"Corp - Rehab Financial Analyst",
"Corp - Rehab Help Desk Analyst",
"Corp - Rehab IS Analyst/Project Manager",
"Corp - Rehab Operations Officer (ROO)",
"Corp - Rehab Vice President of Business Development",
"Corp - Rehab Vice President of Clinical Services",
"Corp - Rehab Vice President of Operations",
"Corp - Rehab Vice President of Strategic Operations",
"Corp - Special Projects - Rehab Services Manager",
"Director of Medical Review - Rehab",
"Director of Rehab",
"HomeNow - Director of Rehab",
"President - Rehab",
"Rehab - Director of Clinical Services (DOC)",
"Rehab - Occupational Therapist",
"Rehab - Occupational Therapist Assistant",
"Rehab - Physical Therapist",
"Rehab - Physical Therapist Assistant",
"Rehab - Regional Operations Officer",
"Rehab - Rehab Services Manager",
"Rehab - Respiratory Therapist",
"Rehab - Restorative Aide",
"Rehab - Restorative Nurse Manager",
"Rehab - Speech Language Pathologist",
"Rehab - Therapist Assistant",
"Rehab Financial & IS Analyst",
"Rehab IS Analyst",
"Rehab Operations & Clinical Specialist",
"Rehab SVC Manager - COTA",
"Rehab SVC Manager - LPTA",
"Rehab SVC Manager - OT",
"Rehab SVC Manager - PT",
"Rehab SVC Manager - ST",
"Sample Job - Rehab / Therapy",
"Senior Rehab HR Generalist",
"Vice President of HR Rehab"};

var orgs = ctx.Organization;


var laTextv1 = "<br><p style=\"color: red;\">Signature HealthCARE is a family-based healthcare company that offers integrated services in 10 states across the continuum of care: skilled nursing, rehabilitation, assisted living, memory care, home health, cognitive care, and telemedicine.</p><br><p style=\"color: red;\">A growing number of our centers are earning quality assurance accreditation and pioneering person - directed care.Many of our skilled nursing facilities have achieved a 4 or 5 - star overall rating from the Centers for Medicare & Medicaid Services.Additionally, we have been awarded as a certified Great Place to Work for three years in a row and Modern Healthcare’s “Best Places to Work”!</p>";
var laTextv4 = "<p style=\"\">Signature HealthCARE is a family-based healthcare company that offers integrated services in 10 states across the continuum of care: skilled nursing, Rehabilitation, assisted living, memory care, home health, cognitive care, and telemedicine.</p><p style=\"\">A growing number of our centers are earning quality assurance accreditation and pioneering person - directed care.Many of our skilled nursing facilities have achieved a 4 or 5 - star overall rating from the Centers for Medicare &amp; Medicaid Services.Additionally, we have been awarded as a certified Great Place to Work for three years in a row and Modern Healthcare’s “Best Places to Work”!</p>";
var laTextv2 = "Signature HealthCARE is a family-based healthcare company that offers integrated services in 10 states across the continuum of care: skilled nursing, rehabilitation, assisted living, memory care, home health, cognitive care, and telemedicine.";
var laTextv3 = "A growing number of our centers are earning quality assurance accreditation and pioneering person - directed care.Many of our skilled nursing facilities have achieved a 4 or 5 - star overall rating from the Centers for Medicare & Medicaid Services.Additionally, we have been awarded as a certified Great Place to Work for three years in a row and Modern Healthcare’s “Best Places to Work”!";

orgs.Where(o => o.LocationJobDescription.Contains(laTextv1)).Pipe(o => o.LocationJobDescription = o.LocationJobDescription.Trim().Replace(laTextv1, ""));
ctx.SaveChanges();

orgs.Where(o => o.LocationJobDescription.Contains(laTextv2)).Pipe(o => o.LocationJobDescription = o.LocationJobDescription.Trim().Replace(laTextv2, ""));
ctx.SaveChanges();

orgs.Where(o => o.LocationJobDescription.Contains(laTextv3)).Pipe(o => o.LocationJobDescription = o.LocationJobDescription.Trim().Replace(laTextv3, ""));
ctx.SaveChanges();

orgs.Where(o => o.LocationJobDescription.Contains(laTextv4)).Pipe(o => o.LocationJobDescription = o.LocationJobDescription.Trim().Replace(laTextv4, ""));
ctx.SaveChanges();

var jobs = ctx.Job.Where(j => !j.IsDeprecated)
	.Where(j => !excludeJobs.Contains(j.JobName)).AsEnumerable();


var jobAdCheckv1 = "memory care, home health, cognitive care, and telemedicine.";
var jobAdCheckv2 = "Place to Work for three years in a row and Modern Healthcare’s ";

var jobAdText1 = "Signature HealthCARE is a family-based healthcare company that offers integrated services in 10 states across the continuum of care: skilled nursing, rehabilitation, assisted living, memory care, home health, cognitive care, and telemedicine.";
var jobAdText2 = "A growing number of our centers are earning quality assurance accreditation and pioneering person - directed care. Many of our skilled nursing facilities have achieved a 4 or 5-star overall rating from the Centers for Medicare & Medicaid Services. Additionally, we have been awarded as a certified Great Place to Work for three years in a row and Modern Healthcare’s \"Best Places to Work\"!";

//Update Textile Job Ads
jobs.Where(j => !j.QuickDescription.IsEmpty() && !j.QuickDescription.Trim().StartsWith("<"))
.Where(j => !j.QuickDescription.Contains(jobAdCheckv1) && !j.QuickDescription.Contains(jobAdCheckv2))
.Pipe(j => j.QuickDescription = j.QuickDescription.Trim() + $"p. {jobAdText1} \n\n p. {jobAdCheckv2}");

//update Html Job Ads
jobs.Where(j => !j.QuickDescription.IsEmpty() && j.QuickDescription.Trim().StartsWith("<"))
.Where(j => !j.QuickDescription.Contains(jobAdCheckv1) && !j.QuickDescription.Contains(jobAdCheckv2))
.Pipe(j => j.QuickDescription = j.QuickDescription + $"<br><p>{jobAdText1}</p><p>{jobAdText2}</p>");
ctx.SaveChanges();

//Add to blank Job Descriptions
jobs.Where(j => j.QuickDescription.IsEmpty()).Pipe(j => j.QuickDescription = $"<br><p>{jobAdText1}</p><p>{jobAdText2}</p>");
ctx.SaveChanges();


