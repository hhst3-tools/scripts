<Query Kind="Statements">
  <Connection>
    <ID>234a9303-3ad4-4d60-962d-63afc17a7a23</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Test\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>

var ctx = Connection.GetSystemUserToken(ClientIds.NorthShoreHealthcareAccount).GetCoreContext();

//NSH Reported that their job highlights could not be edited
//To resolve we had to find a way for the curser to get inserted before the NSHC1 (Hidden Tracking code)

//Find the bad HTML that needs to be replaced (run a query or review the job in console and look at the code in the WYSIWYG Editor
var oldHtml = "<div style=\"opacity: 0; font-size:4px;\"><p>NSHC1</p></div>";
var oldHtml2 = "<div style=\"opacity: 0; font-size:4px;\"><h2>NSHC1</h2></div><div><span style=\"opacity: 0\">HC1</span></div>";

//Find fix, in this case we needed to put a div with a space, the curse looks for the first HTML element in the code 
var newHtml = "<div><span style=\"font-size: 1rem;\">&nbsp;</span></div><div><span style=\"opacity: 0\">NSHC1</span></div>";

//OrganizationJob is the table for the job at a specific organization. Specifics is the job highlights text area in console
//Pipe creates the change to the record

ctx.OrganizationJob.Where(oj => oj.Specifics.Contains(oldHtml)).Pipe(oj => oj.Specifics = oj.Specifics.Replace(oldHtml, newHtml));
ctx.OrganizationJob.Where(oj => oj.Specifics.Contains(oldHtml2)).Pipe(oj => oj.Specifics = oj.Specifics.Replace(oldHtml2, newHtml));

//Save database changes
ctx.SaveChanges();

