<Query Kind="Statements">
  <Connection>
    <ID>234a9303-3ad4-4d60-962d-63afc17a7a23</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Test\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>

var ctx = Connection.GetSystemUserToken(ClientIds.SignatureHealthcareAccount).GetCoreContext();

var homeNowIds = ctx.Organization.Where(o => o.ParentOrgId == new Guid("8237446b-e598-4470-95d4-b7a16d632f80") ).Select(o => o.OrganizationId).ToList().Dump();

var removeText1 = @"Signature HealthCARE is a family-based healthcare company that offers integrated services in 10 states across the continuum of care: skilled nursing, rehabilitation, assisted living, memory care, home health, cognitive care, and telemedicine.";
var removeText2 = @"A growing number of our centers are earning quality assurance accreditation and pioneering person - directed care. Many of our skilled nursing facilities have achieved a 4 or 5-star overall rating from the Centers for Medicare & Medicaid Services. Additionally, we have been awarded as a certified Great Place to Work for three years in a row and Modern Healthcare’s ""Best Places to Work""!";
var homeNowJobsRemove = ctx.OrganizationJob.Where(oj => homeNowIds.Contains(oj.OrganizationId)).Where(oj => oj.Job.QuickDescription.Contains(removeText1) && oj.Job.QuickDescription.Contains(removeText2)).AsEnumerable();
var htmlJobs = ctx.OrganizationJob.Where(oj => homeNowIds.Contains(oj.OrganizationId)).Where(oj => oj.Job.QuickDescription.Contains(removeText1) && oj.Job.QuickDescription.Contains(removeText2)).Where(oj => oj.Job.QuickDescription.StartsWith("<") );
var textTileJobs = ctx.OrganizationJob.Where(oj => homeNowIds.Contains(oj.OrganizationId)).Where(oj => oj.Job.QuickDescription.Contains(removeText1) && oj.Job.QuickDescription.Contains(removeText2)).Where(oj => !oj.Job.QuickDescription.StartsWith("<") );


homeNowJobsRemove.Pipe(j => j.Job.QuickDescription = j.Job.QuickDescription.Trim().Replace(removeText1, "").Replace(removeText2, ""));
ctx.SaveChanges();

var oldEEO = @"Signature HealthCARE is an Equal Opportunity-Affirmative Action Employer – Minority / Female / Disability / Veteran and other protected categories.";
var newEEO = @"Signature HomeNow is an Equal Opportunity-Affirmative Action Employer – Minority / Female / Disability / Veteran and other protected categories.";
var jobsWithOldEEO = ctx.OrganizationJob.Where(oj => homeNowIds.Contains(oj.OrganizationId) ).Where(oj => oj.Job.QuickDescription.Contains(oldEEO)).AsEnumerable();

jobsWithOldEEO.Pipe(oj => oj.Job.QuickDescription = oj.Job.QuickDescription.Trim().Replace(oldEEO, newEEO));
ctx.SaveChanges();