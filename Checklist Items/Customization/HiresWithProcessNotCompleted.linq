<Query Kind="Statements">
  <Connection>
    <ID>1b4d0ffe-df90-480d-b80e-5cbf12d8f849</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Prod\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <IsProduction>true</IsProduction>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>


var ctx = Connection.GetSystemUserToken(ClientIds.LifeCareCentersofAmericaAccount).GetCoreContext();

var date = new DateTime(2020, 08, 01);

var hires = ctx.ApplicationJobOrganization.Where(ajo => ajo.Offer.StartDate >= date).Where(ajo =>  ajo.Process.Any(p => p.ProcessDefinitionId == new Guid("3fd313d7-699d-4a8d-8fff-9b2141118aae") && p.StatusCode != Vikus.Data.Client.ProcessStatusCode.Completed)).Select(ajo => new {
  ajo.Organization.OrganizationName,
  ajo.Job.JobName,
  ajo.Application.Candidate.FirstName,
  ajo.Application.Candidate.LastName,
  HireDate = ajo.Offer.StartDate,
  Status = ajo.Process.Where(p => p.ProcessDefinitionId == new Guid("3fd313d7-699d-4a8d-8fff-9b2141118aae")).First().StatusCode
}).ToArray();

Util.WriteCsv(hires, @"C:\code\LCCACovid.csv" );