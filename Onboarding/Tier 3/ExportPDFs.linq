<Query Kind="Statements">
  <Connection>
    <ID>1b4d0ffe-df90-480d-b80e-5cbf12d8f849</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Prod\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <IsProduction>true</IsProduction>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>


var ctx = Connection.GetSystemUserToken(ClientIds.LegacyHealthServicesAccount).GetContext();


var foo = ctx.OnboardingPDFForm
	.Select(x => new { x.Name, 
		form = x.OnboardingPDFFormVersion.OrderByDescending(y => y.EffectiveDate).FirstOrDefault() 
	})
	.Where(x => x.form != null && !x.form.IsEmptyVersion == true)
	.Select(x => new
	{
		x.Name,
		x.form.EffectiveDate,
		x.form.PDFForm
	})
	.ToArray()
	.Map(x =>
	{
		var path = @"C:\code\OnboardingDocs\";
		Directory.CreateDirectory(path);
		if (x.Name.Contains(@"8850\LTU"))
		{
			var newName = x.Name.Replace(@"8850\LTU", "8850LTU");
			File.WriteAllBytes(Path.Combine(path, $"{newName} - {x.EffectiveDate.ToString("MM-dd-yy")}.pdf"), x.PDFForm);
			
		} else if (x.Name.Contains(@"8850/LTU  - Hillside ")) {
			var newName = x.Name.Replace("8850/LTU  - Hillside ", "8850 LTU  - Hillside");
			File.WriteAllBytes(Path.Combine(path, $"{newName} - {x.EffectiveDate.ToString("MM-dd-yy")}.pdf"), x.PDFForm);
		}
		else if (x.Name.Contains("\\"))
		{
			var newName = x.Name.Replace("\\", " ");
			File.WriteAllBytes(Path.Combine(path, $"{newName} - {x.EffectiveDate.ToString("MM-dd-yy")}.pdf"), x.PDFForm);
		}
		else if (x.Name.Contains("/"))
		{
			var newName = x.Name.Replace("/", " ");
			File.WriteAllBytes(Path.Combine(path, $"{newName} - {x.EffectiveDate.ToString("MM-dd-yy")}.pdf"), x.PDFForm);
		}
		else {
			File.WriteAllBytes(Path.Combine(path, $"{x.Name} - {x.EffectiveDate.ToString("MM-dd-yy")}.pdf"), x.PDFForm);
		}
		
		
	});




