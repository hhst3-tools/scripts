<Query Kind="Statements">
  <Connection>
    <ID>1b4d0ffe-df90-480d-b80e-5cbf12d8f849</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Driver Assembly="EF7Driver" PublicKeyToken="469b5aa5a4331a8c">EF7Driver.StaticDriver</Driver>
    <CustomAssemblyPath>C:\codelib\netstandard2.1\Vikus.Data.Auth.dll</CustomAssemblyPath>
    <CustomTypeName>Vikus.Data.Auth.AuthEntities</CustomTypeName>
    <AppConfigPath>C:\code\misctools\LinqpadConfig\Prod\appsettings.json</AppConfigPath>
    <DisplayName>HHS</DisplayName>
    <IsProduction>true</IsProduction>
    <DriverData>
      <UseDbContextOptions>false</UseDbContextOptions>
    </DriverData>
  </Connection>
  <Namespace>Vikus.BusinessLogic.SPS</Namespace>
  <Namespace>Vikus.Core</Namespace>
  <Namespace>Vikus.Data.Auth</Namespace>
  <Namespace>Vikus.Data.Framework</Namespace>
  <Namespace>Z.EntityFramework.Plus</Namespace>
</Query>

var id = "4589a452-6334-4351-9305-fa2f99b133cc";



var guidList = new List<string>() {
	"4589a452-6334-4351-9305-fa2f99b133cc",
"8c26ee2c-7860-4dad-bf8f-f13ecdaa3a4a",
"d290cdd5-f56a-49e0-bb9f-e83ce31dc870",
"dfdcacdb-5bc9-4bdb-a250-d5cb7b49ef54",
"7911affa-4077-498a-9db3-d48b244cac46",
"ee0deb13-62e8-4837-878c-d3f2085722e6",
"82540fd8-1af7-448f-b88b-c84c043f846c",
"b01a8fdc-cc1d-4b9e-82ea-be8c2c0a1702",
"f597907f-7d4e-4e9b-aef9-b074148a38df",
"c6202942-2bbe-4401-96ff-a939534fdbc6",
"b01d143e-ce8f-406f-9e22-a7a11dea0ee6",
"66e81956-e334-4e2b-b6b1-a3e96e106aed",
"ae094670-b498-4a84-b41f-99a2ed90c5b5",
"4d9d20f7-5dce-49a7-9dbf-92d6e11596ff",
"148ba4c1-4b60-4835-9d49-92008e616a5e",
"706b8e79-a007-4fc0-a1fe-8ae797eb9f73",
"df4f1215-6540-4724-ade2-88a0b1f64f78",
"30e3ae0a-75e4-43c0-a58d-86917664c5c4",
"1c521d40-1d64-4664-8171-848529d0cd63",
"606cce33-2a49-4a7a-8e70-7da737fb1376",
"9fb344c7-a9f3-4b00-9c77-7be71ece2e84",
"5d7f603a-89b2-4602-8707-79560496d893",
"c34049dd-ad4f-4c81-8e79-77e7f525507d",
"85a0ea07-b987-4648-9c2d-6764b17d614c",
"85042a55-c032-4b26-a5ea-62fdb823efa8",
"d10443fd-b6be-48de-a6d0-5f62884f17bc",
"e2d365f9-c05c-40ea-856f-5a03737515d7",
"58766e10-6e71-443d-88de-574efeddbbb9",
"7d47f913-78f4-40e2-b158-535a970ede2a",
"85c9d78d-58d9-4e61-836f-50bea0243fba",
"61426f3b-b61e-44a1-ae1e-4a91e1a29b18",
"b65a422d-6703-4327-8df9-42d669edd8a1",
"52380a42-ff39-43ff-8599-3fa5060477c9",
"2895a51d-3aa8-48bd-b1b9-353368ec95c9",
"4e8fabd8-7683-4668-86e5-313bb63ec320",
"b7f219bf-ed30-4b1f-a77d-2d2dfaf1d38c",
"0195b4e4-5e60-4f61-b3aa-294b2e0d0e56",
"0c2d6889-fea8-4afb-b96f-210e8999865b",
"a503687a-05d0-4c83-a220-1e799e571843",
"b1ab5ff8-85d7-4713-a660-1c7c17b9f69c",
"6f3e92ff-40f4-47e2-817d-1ab29651f6fd",
"2be0aea1-8fce-4597-81c1-18f9231b10e9",
"758e4026-69c7-4e81-95b3-10c9243d8166",
"8b881389-af0b-4c53-b90c-0ffcc443be0a",
"b8f10c36-8006-42c3-b6e6-0c3445818e8b",
"c65125b1-1e46-4020-bcda-0b4ead57d738",
"3c8cbaad-df33-4f8e-b374-033aa15d5772" };

guidList.Map(l =>
{
	var url = @$"https://c-master.hcsqa.com/applications-v2/{new Guid(l).ToCompactString()}/documents?cId=hKxnTA4tqkewOQqWORizoQ".Dump();


var = @$"https://c-master.hcsqa.com/console/api/documents/3UlAw0-tgUyOeXfn9SVQfQ?download=true&docs=4iuQRxnTDEOB7R4vfHemhA%2CApplication%20Report%2CsS80yiKiMUaaNnZjc2Q9Cw%2CTykVkFn6pEGp9lXqU9ADOA%2C-XykK3ZKIk22x-2ALL1FYw%2C9nH_FJBwYE2n9qgO2dO31A"

});

